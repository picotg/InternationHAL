function encodeGetParams (p) {
   return Object.entries(p).map(kv => kv.map(encodeURIComponent).join("=")).join("&");
} 
 
var params = {};
var baseURLs = {};
 
function update_params(idLink) {
   $('#' + idLink).attr("href", baseURLs[idLink] + encodeGetParams(params[idLink]));
}
 
function add_bin_check(idOption, idLink) {
   option = ($('#' + idOption).data('option')!=undefined)?$('#' + idOption).data('option'):idOption;
   getChanger = (option, idLink) => function() {
      change_bin_option(option, this.checked, idLink);
   }
   $('#' + idOption).change(getChanger(option, idLink));
   change_bin_option(option, $('#' + idOption).attr('checked'), idLink);
   update_params(idLink);
}
 
 
function change_bin_option(option, value, idLink) {
   if (value) {
      params[idLink][option] = true;
   } else {
      delete params[idLink][option];
   }
   update_params(idLink);
}

function loadParametricLink() {
   $('.parameterLink').each((index, element) => {
      idLink = $(element).attr('id');
      baseURLs[idLink] = $('#' + idLink).attr("href");
      if(baseURLs[idLink].slice(-1)!='&'&&baseURLs[idLink].slice(-1)!='?') {
         baseURLs[idLink] += '?';
      }
      targetCheck = ($('#' + idLink).data("target")!=undefined)?$('#' + idLink).data("target"):idLink;
      params[idLink] = {};
      $('.'+targetCheck+'Param').each((index, element) => {
         add_bin_check($(element).attr('id'), idLink);
      })
   });
}
 
$(document).ready(function() {
   loadParametricLink();
})
 