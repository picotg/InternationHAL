/**
 * Générateur d'info utilisant liquid js
 */
class HalInfoLiquidRender extends HalInfoRender {
    // moteur liquid js
    #engine

    /**
     * constructeur
     * @param {string} rootDir répertoire des templates
     */
    constructor(rootDir = 'views/') {
        super();
        this.#engine = new liquidjs.Liquid({
            root: [rootDir],
            extname: '.liquid'
        });
    }

    /**
     * génére les info à afficher pour le template name avec les données data.
     * 
     * @param {string} name nom du modèle à utilisé
     * @param {Object} data donnée a utiliser sur le modèle
     * @returns {string} info rendu avec les données
     */
    async render(name, data) {
        return await engine.renderFile(name, data);
    }
}

var halInfoRender = new HalInfoLiquidRender();
