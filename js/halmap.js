// met en majuscule la première lettre
capitalize1L = value => value[0].toUpperCase() + value.substring(1);

function createScale(values, numberPart) {
    var sizePart = Math.floor(values.length/numberPart);
    sizePart += (values.length%numberPart!==0)?1:0;
    var resultat = [];
    var previous = 0;
    var currentIndex = 0;
    var newValue = 0;
    for (var i = 0; i < numberPart; i++) {
        currentIndex = i * sizePart;
        newValue = (values[currentIndex] !== undefined)?values[currentIndex]:previous+1;
        if(newValue <= previous) newValue = previous+1;
        resultat.push(newValue);
        previous = newValue;
    }
    return resultat;
}

/**
 * Générateur d'info à afficher sans dépendance
 */
class HalInfoRender {
    /**
     * génére les info à afficher pour le template name avec les données data.
     * 
     * @param {string} name nom du modèle à utilisé
     * @param {Object} data donnée a utiliser sur le modèle
     * @returns {string} info rendu avec les données
     */
    async render(name, data) {
        var result = ''
        switch(name) {
            case 'info/infoInstitution':
                result = '<h3>' + data.name + '</h3><br><br>';
                result += 'nombre d\'institutions participantes : ' + data.institutions.length;
                break
            case 'info/infoDomain':
                result = '<h3>' + data.name + '</h3><br><br>';
                result += 'nombre de domaines : ' + data.domains.length;
                break
            case 'info/infoDefault':
                result = 'Passer la souris sur un pays'
                break
            case 'popup/popupInstitution':
                result = '<h3>' + data.name + '</h3><br><ul>';
                for(const institution of data.institutions) {
                    result += '<li>'+ institution.name.replace("%%simplecote%%", "'") +'</li>'
                }
                result += '</ul>'
                break
            case 'popup/popupDomain':
                result = '<h3>' + data.name + '</h3><br><ul>';
                for(const domain of data.domains) {
                    result += '<li>'+ domain.name +' : ' + domain.nb +'</li>'
                }
                result += '</ul>'
                break
            case 'popup/popupCrossDomain':
                result = '<h3>' + data.name + '</h3><br><ul>';
                for(const crossDomain in data.crossDomains) {
                    result += '<li>'+ crossDomain + ' : ' + data.crossDomains[crossDomain].nb +'</li>'
                }
                result += '</ul>'
                break
        }
        return result;
    }
}

/**
 * gestion des intéraction (affichage des données, demande, ...) sans dépendance
 */
class InteractInfo {
    /**
     * affiche le message avec le titre.
     * @param {string} message message à afficher
     * @param {string} title titre du message
     */
    async showMessage(message, title) {
        // enlever le HTML
        var temporaryElement = document.createElement("div");
        temporaryElement.innerHTML = message;
        message = temporaryElement.textContent || temporaryElement.innerText || "";
        alert(title + '\n' + message);
    }
}

/**
 * gestion du mode de visualisation
 */
class VisualizerMode {
    // nom du mode
    #modeName
    // list détailler
    #listDetails = {
        list: [],
        infos: {},
    }
    // échelle
    #values
    // couleurs pour l'échelle
    #colorScale = ['#800026', '#BD0026', '#E31A1C', '#FC4E2A', '#FD8D3C', '#FEB24C', '#FED976', '#FFEDA0'];
    // list des titres de légende selon mode
    #legendTitles = {
        domain: "nombre de domaine",
        crossDomain: 'pluridisciplinarité',
        institution: 'nombre d\'institutions'
    }
    // limité les domaines au nivau 0
    #domainL0

    /**
     * échelle
     * 
     * @type {number[]}
     */
    get scale() {
        return createScale(this.#values, this.#colorScale.length)
    }

    set values(values) {
        this.#values = values;
    }

    /**
     * constructeur
     * 
     * @param {string} modeName nom du mode d'affichage
     * @param {string} idMap id de l'élément HTML à utiliser
     * @param {string[]} colorScale tableau des couleurs de l'échelle
     * @param {boolean} domainL0 limité le domaine au niveau 0
     */
    constructor(modeName, idMap, colorScale, domainL0) {
        this.#modeName = modeName;
        this.#listDetails.id = $(idMap).data(modeName.toLowerCase() + 'listid');
        this.#colorScale = colorScale;
        this.#domainL0 = domainL0;
        if($(idMap).data('legendTitles')!==undefined) {
            this.#legendTitles = $(idMap).data('legendTitles');
        }
    }

    /**
     * couleurs de l'échelle
     * 
     * @type {string[]}
     */
    get colorScale() {
        return this.#colorScale;
    }

    /**
     * nom du template pour l'affichage des info d'un pays
     * 
     * @type {string}
     */
    get info() {
        return 'info/info'+capitalize1L(this.#modeName);
    }

    /**
     * nom du template pour l'affichage des poppup suite au click sur la map
     * 
     * @type {string}
     */
    get popup() {
        return 'popup/popup'+capitalize1L(this.#modeName);
    }

    /**
     * liste détailler pour chaque élément (ex : liste des institution avec pays concerné et nombre de documents concerné)
     * 
     * @type {Object[]}
     */
    get listDetails() {
        return this.#listDetails;
    }

    /**
     * titre de la légende du mode en cours
     * 
     * @type {string}
     */
    get legendTitle() {
        return this.#legendTitles[this.#modeName];
    }

    /**
     * nom du mode
     * 
     * @type {string}
     */
    get modeName() {
        return this.#modeName
    }


    /**
     * filtre pour le choix de l'affichage du pays
     * 
     * @type {CallableFunction}
     */
    get filter() {
        var propName = this.modeName + 'Number';
        if (this.modeName === 'crossDomain') {
            propName = this.modeName + (this.#domainL0?'L0':'') + 'Number';
        }
        return feature => feature.properties[propName] >= 1;
    }

    /**
     * Ajoute les infos d'une feature à la liste detaillés
     * 
     * @param {Object} feature feature à ajouter
     */
    addDetailOnList(feature) {
        if(this.modeName !== 'crossDomain') {
            feature.properties[this.modeName + 's'].forEach(element => {
                if(!this.#listDetails.list.includes(element.name)) {
                    this.#listDetails.list.push(element.name);
                    this.#listDetails.infos[element.name] = {
                        'countries': [feature.properties.name],
                        'nb': element.nb,
                        'element': element
                    }
                } else {
                    this.#listDetails.infos[element.name]['countries'].push(feature.properties.name);
                    this.#listDetails.infos[element.name]['nb'] += element.nb;
                }
            })
        } else {
            var fullModName = 'crossDomains' + (this.#domainL0?'L0':'');
            if(Object.keys(feature.properties).includes(fullModName)) {
                Object.keys(feature.properties[fullModName]).forEach(key => {
                    if(!this.#listDetails.list.includes(key)) {
                        this.#listDetails.list.push(key);
                        this.#listDetails.infos[key] = {
                            'countries': [feature.properties.name],
                            'countriesId': [feature.id],
                            'names': feature.properties[fullModName][key].names,
                        }
                    } else {
                        this.#listDetails.infos[key]['countries'].push(feature.properties.name);
                        this.#listDetails.infos[key]['countriesId'].push(feature.id);
                    }
                })
            }
        }
    }

    /**
     * Renvoi la couleur pour une feature donnée
     * 
     * @param {Object} feature feature à afficher
     * @returns {string} couleur d'affichage
     */
    featureColor(feature) {
        var modeName = this.modeName;
        if (modeName === 'crossDomain') {
            modeName += this.#domainL0?'L0':'';
        }
        return this.color(feature.properties[modeName + 'Number'])
    }

    /**
     * Renvoi la couleur sur l'échelle pour une valeur numérique donnée
     * 
     * @param {number} d valeur numérique
     * @returns {string} couleur
     */
    color(d) {
        var nbValue = this.colorScale.length;
        var genIndexValue = nbValue => Array.from({length: nbValue}, (x, i) => nbValue-i);
        var indexValues = genIndexValue(nbValue);
        for (var i=0; i < nbValue; i++) {
            if(d > this.scale[indexValues[i]]) return this.colorScale[i];
        }
        return this.colorScale[nbValue-1];
    }
}

/**
 * Carte Hal
 */
class HalMap {
    /** id de l'élément HTML de la carte */
    #idMap
    /** couleurs pour l'échelle */
    #colorScale = ['#800026', '#BD0026', '#E31A1C', '#FC4E2A', '#FD8D3C', '#FEB24C', '#FED976', '#FFEDA0'];
    /** tilemap au format de tilemaps.json (url, options) */
    #tilemap = {
        "url": "https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png",
        "options": {
            "attribution": "© Openstreetmap France | © OpenStreetMap contributors",
            "noWrap": true,
            "maxZoom": 4
        }
    };
    /** mode de visualisation */
    #visualizerMode
    /** controles sur la carte */
    #controls = {};
    /** besoin de recharger ou non la tilemap */
    #fistLoadTilemap = true;
    /** carte leaflet */
    #map
    /** geoJSON data */
    #geojson
    /** déactivation de l'hiligthing */
    #deactivateHighlight = false
    /** hiligth courrent */
    #currentTarget = null
    /** la sourrie est dans un cadre info */
    #inInfo = false
    /** parametre from data source */
    #parameter = null
    /** values pour visualizerMode */
    #values
    /** limité les domaines au nivau 0 */
    #domainL0 = false

    /**
     * limité les domaines au nivau 0
     * 
     * @type {boolean}
     */
    get domainL0() {
        return this.#domainL0;
    }

    set domainL0(value) {
        this.#domainL0 = value;
        this.mode = this.#visualizerMode.modeName
    }


    /**
     * souris présente dans le cadre info
     * 
     * @type {boolean}
     */
    get inInfo() {
        return this.#inInfo;
    }

    set inInfo(value) {
        this.#inInfo = value;
    }

    /**
     * liste des controles présent sur la carte
     * 
     * @type {Object[]}
     */
    get controls() {
        return this.#controls;
    }

    /**
     * geoJSON afficher par la carte
     * 
     * @type {Object}
     */
    get geojson() {
        return this.#geojson;
    }

    set geojson(geojson) {
        this.#geojson = geojson;
    }

    /**
     * parametre de la requête Hal
     * 
     * @type {Object}
     */
    get parameter() {
        return this.#parameter
    }

    set parameter(parameter) {
        this.#parameter = parameter
    }

    set values(values) {
        this.#values = values
    }

    /**
     * Carte leaflet
     * 
     * @type {Object}
     */
    get map() {
        return this.#map
    }


    /**
     * Tilemap (URL et option)
     * 
     * @type {Object}
     */
    get tilemap() {
        return this.#tilemap;
    }

    /**
     * Mode de visualisation
     * 
     * @type {VisualizerMode}
     */
    get mode() {
        return this.#visualizerMode;
    }

    set mode(modeName) {
        this.#visualizerMode = new VisualizerMode(modeName, this.#idMap, this.#colorScale, this.#domainL0);
        if (modeName === 'crossDomains') {
            modeName += this.#domainL0?'L0':'';
        }
        this.#visualizerMode.values = this.#values[modeName];
    }

    /**
     * Pays courament afficher
     * 
     * @type {Object}
     */
    get currentTarget() {
        return this.#currentTarget
    }

    set currentTarget(newTarget) {
        this.#currentTarget = newTarget;
    }

    /**
     * Constructeur
     * 
     * @param {string} idMap id de l'élément HTML utilisé pour afficher la carte
     */
    constructor(idMap) {
        this.#idMap = idMap;
        this.#map = L.map($(idMap).attr('id')).setView([40, 2.3], 3);
    }

    /**
     * remet le style de tous les pays à 0
     */
    resetAllStyle() {
        var map = this.#map;
        map.eachLayer(function(layer) {
            if (layer.feature!=undefined) {
                map.geoJSONLayer.resetStyle(layer);
            }
        })
    
    }

    /**
     * Mets en surbrillance le pays voulu
     * 
     * @param {string} country code à 2 lettres du pays
     */
    highlightCountry(country) {
        this.#map.eachLayer(function(layer) {
            if (layer.feature!=undefined&&layer.feature.id==country) {
                layer.setStyle({
                    weight: 2,
                    color: '#000',
                    dashArray: '',
                    fillOpacity: 0.7
                });
                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront(); // Permet de garantir que le pays est au-dessus des autres couches de données
                }
            }
        })
    
    }

    /**
     * supprime les controles pour infos et légendes
     */
    #cleanControls() {
        this.#map.removeControl(this.#controls.info);
        this.#controls.info.remove();
        this.#map.removeControl(this.#controls.legend);
        this.#controls.legend.remove();
    }


    /**
     * modifie la tilemap (URL et option) depuis un fichier 
     * @param {string} name nom des tilemap
     * @param {string} tilemapsFile fichier des tilemaps
     */
    async setTilemapFromFile(name="default", tilemapsFile='data/tilemaps.json') {
        var data = await (await fetch(tilemapsFile)).json();
        this.#tilemap = data[name];
    }

    /**
     * initialise les controle pour la légendes et les infos
     */
    initControls() {
        if(this.#controls.legend !== undefined) this.#cleanControls();
        var map = this.#map;
        var halMap = this;
        var domainL0 = this.#domainL0;

        // création ou nétoyage des controles de la carte
        function reset(target) {
            map.geoJSONLayer.resetStyle(target);
            if(target==halMap.currentTarget) {
                this.update();
            }
        }
    
        if(!this.#deactivateHighlight) {
            // ajout légende textuelle
            this.#controls.info = L.control();
            this.#controls.info.onAdd = function () {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
            }

            // construit la fonction de mise à jour des infos affiché
            function update (props) {
                var new_props = props;
                if ((halMap.mode.modeName === 'crossDomain') && domainL0 && props !== undefined) {
                    new_props = {crossDomains: props.crossDomainsL0}
                }
                halMaps.generateInfo(new_props, halMap.mode.info).then(infos => {
                    this._div.innerHTML = infos;
                })
            }
            this.#controls.info.update = update;
            this.#controls.info.reset = reset;
            this.#controls.info.addTo(map);
            $(this.#controls.info._div).on('mouseover', () => {
                this.#inInfo = true;
            })
            $(this.#controls.info._div).on('mouseout', () => {
                this.#inInfo = false;
                if(halMap.currentTarget) {
                    this.#controls.info.reset(halMap.currentTarget);
                }
            })
        }

        //ajout légende couleurs
        this.#controls.legend = L.control({position: 'bottomright'});

        this.#controls.legend.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'info legend'),
                grades = halMap.mode.scale;
            div.innerHTML += '<h3>' + halMap.mode.legendTitle + '</h3>';
            for (var i = 0; i < grades.length; i++) {
                div.innerHTML += '<i style="background:' + halMap.mode.color(grades[i] + 1) + '"></i> ' +
                    grades[i] + (grades[i + 1] ? ' – ' + grades[i + 1] + ' <br>' : '+');
            }
            return div;
        }
        this.#controls.legend.addTo(map);
    }

    /**
     * charger les données depuis les data, un fichier ou une API.
     */
    async loadData() {
        var idMap = this.#idMap
        var map = this.#map
        if ($(idMap).data('source') !== undefined) {
            await $.ajax({
                url:$(idMap).data('source'),
                success: async data => {
                    await $(idMap).data('values', data['values']);
                    await $(idMap).data('geojson', data['map']);
                    await $(idMap).data('parameter', data['parameter']);
                }
            })
        }
        this.#geojson = $(idMap).data('geojson');
        this.#parameter = $(idMap).data('parameter');
        this.#values = $(idMap).data('values');
        if($(idMap).data('colorscale')!==undefined) {
            this.#colorScale = $(idMap).data('colorscale');
        }
        this.#domainL0 = ($(idMap).data('domainl0') !== undefined)?$(idMap).data('domainl0'):false;
        var modeName = $(idMap).data('mod');
        if(modeName === undefined) modeName = 'institution';
        this.mode = modeName;
        this.#deactivateHighlight = $(idMap).data('deactivatehighlight')?true:false;
        var reloadTilLayer = true;
        if($(this.#idMap).data('tilemapfile')!==undefined||$(this.#idMap).data('tilemapname')!==undefined) {
            reloadTilLayer = false;
            if (halMaps.tilemapNames[this.#idMap]!==$(this.#idMap).data('tilemapname')||halMaps.tilemapNames[this.#idMap]===undefined) {
                halMaps.tilemapNames[this.#idMap] = ($(this.#idMap).data('tilemapname')!==undefined)?$(this.#idMap).data('tilemapname'):'default';
                reloadTilLayer = true;
            }
            await this.setTilemapFromFile(halMaps.tilemapNames[this.#idMap], $(this.#idMap).data('tilemapfile'));
        } else if($(this.#idMap).data('tiledata')!==undefined) {
            this.#tilemap = JSON.parse($(this.#idMap).data('tiledata'));
        }
        map.eachLayer(function (layer) {
            if(!(layer instanceof L.TileLayer) || reloadTilLayer) {
                map.removeLayer(layer);
            }
        });
        if(reloadTilLayer||this.#fistLoadTilemap) {
            this.#fistLoadTilemap = false;
            // ajout couche openstreetmap
            var osmLayer = L.tileLayer(this.#tilemap.url, this.#tilemap.options);
            map.addLayer(osmLayer);
        }
    }

    /**
     * initialise les observer pour chaque pays
     */
    async initOnEach() {
        var halMap = this;
        var idMap = this.#idMap;
        var domainL0 = this.#domainL0;

        // mise en surbrilliance des pays
        function highlightFeature(e) {
            var layer = e.target;
            halMap.inInfo = false;
            if(halMap.currentTarget) {
                halMap.map.geoJSONLayer.resetStyle(halMap.currentTarget);
            }

            layer.setStyle({
                weight: 2,
                color: '#666',
                dashArray: '',
                fillOpacity: 0.7
            });

            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront(); // Permet de garantir que le pays est au-dessus des autres couches de données
            }

            halMap.currentTarget = layer;
            halMap.controls.info.update(layer.feature.properties);
        }

        // retour à la normal quand la souris quitte un pays
        function resetHighlight(e) {
            setTimeout(function () {
                if(!halMap.inInfo) {
                    halMap.controls.info.reset(e.target);
                }
            }, 1)
        }

        // événement pour le click qur la carte
        async function clickMap (e) {
            var layer = e.target;
            var props = layer.feature.properties;
            props.id = layer.feature.id;
            props.parameter = halMap.parameter;
            switch (props.parameter.queryType) {
                case "collection":
                    props.field = 'collCode_s';
                    break;
                case "structure":
                    props.field = 'structure_t';
                    break;
                case "idhal":
                    props.field = 'authIdHal_i';
                    break;
            }
            props.domainL0 = domainL0;
            var institutions = await halMaps.halInfoRender.render(halMap.mode.popup, props);
            halMaps.interactInfo.showMessage(institutions, props.name);
        }

        // style d'affichage des pays
        function style(feature) {
            return {
                fillColor: halMap.mode.featureColor(feature),
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.7
            };
        }

        // deffini les événement pour chaque pays
        function onEachFeature(feature, layer) {
            halMap.mode.addDetailOnList(feature);
            if(!$(idMap).data('deactivatehighlight')) {
                layer.on({
                    mouseover: highlightFeature,
                    mouseout: resetHighlight,
                    click: clickMap,
                });
            } else {
                layer.on({
                    click: clickMap,
                });
            }
        }

        // charger data
        this.map.geoJSONLayer = await L.geoJson(this.geojson, { 
            style: style,
            onEachFeature: onEachFeature,
            filter: this.mode.filter,
        }).addTo(this.map);
    }
}

/**
 * Lot de cartes présent sur une page
 */
class HalMaps {
    // liste des cartes
    #halMaps = {}
    // halInfoRender
    #halInfoRender = null
    // tilemapnames
    #tilemapNames = {}
    // outil pour afficher des message
    #interactInfo

    /**
     * manager pour les interaction
     * @type {InteractInfo}
     */
    get interactInfo() {
        return this.#interactInfo;
    }

    set interactInfo(newValue) {
        this.#interactInfo = newValue;
    }

    /**
     * Liste des noms de tilemap utiliser
     * @type {string}
     */
    get tilemapNames() {
        return this.#tilemapNames;
    }

    /**
     * moteur de rendu d'info
     * @type {HalInfoRender}
     */
    get halInfoRender() {
        return this.#halInfoRender;
    }

    set halInfoRender(newRender) {
        this.#halInfoRender = newRender;
    }

    /**
     * constructeur
     */
    constructor() {
        this.#halInfoRender = new HalInfoRender();
        this.#interactInfo = new InteractInfo()
    }

    /**
     * Accé à une map dans la liste par son id
     * @param {string} idMap id de la carte
     * @returns {HalMap} map la carte
     */
    getMap(idMap) {
        return this.#halMaps[idMap];
    }

    /**
     * Creer et ajoute une nouvelle map Hal pour l'élément HTML idMap.
     * @param {string} idMap  id élément à transformer en carte
     */
    addMap(idMap) {
        this.#halMaps[idMap] = new HalMap(idMap);
    }

    /**
     * génére les info si un pays et selectionné ou les info par défaut
     * @param {Object} props propriété du feature associé à l'élément
     * @param {string} template nom du modèle à utilisé
     * @returns {string} info générée
     */
    async generateInfo(props, template) {
        if (props) {
            return await this.#halInfoRender.render(template, props);
        } else {
            return await this.#halInfoRender.render('info/infoDefault', {});
        }
    }

    /**
     * charge les différente données d'une carte
     * @param {string} idMap id de la carte
     */
    async loadMap(idMap) {
        if(halMaps.getMap(idMap) === undefined) {
            halMaps.addMap(idMap);
        }

        await halMaps.getMap(idMap).loadData();
        halMaps.getMap(idMap).initControls();
        await halMaps.getMap(idMap).initOnEach();

        halMaps.getMap(idMap).map.whenReady(function() {
            $('#'+halMaps.getMap(idMap).mode.listDetails.id).data(halMaps.getMap(idMap).mode.modeName.toLowerCase()+'details', halMaps.getMap(idMap).mode.listDetails).trigger('changeData');
        });
    }
}

/*
 * tableau des cartes avec l'id comme clef
 */
var halMaps = new HalMaps();

$( document ).ready(function(){
    if (typeof halInfoRender !== 'undefined') {
        halMaps.halInfoRender = halInfoRender;
    }
    if (typeof interactInfo !== 'undefined') {
        halMaps.interactInfo = interactInfo;
    }
    
    function makeChanger(selector, loader) {
        return () => loader(selector);
    }

    // créé une carte pour chaque element de class map
    $('.halmap').each(async function() {
        idSelector = '#'+$(this).attr('id');
        halMaps.loadMap(idSelector);
        $(idSelector).on('changeData', makeChanger(idSelector, halMaps.loadMap));
    })
})