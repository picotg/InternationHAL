/**
 * gestion des intéraction (affichage des données, demande, ...) utilisant JQeury UI
 */
class InteractJqueryUIInfo extends InteractInfo {
    /**
     * affiche le message avec le titre.
     * @param {string} message message à afficher
     * @param {string} title titre du message
     */
    async showMessage(message, title) {
        $('#dialog').html(message);
        $("#dialog").dialog({
            width: "auto",
            title: title
        });
    }
}

var interactInfo = new InteractJqueryUIInfo();
