var testArrays = [
    [0, 0, 0, 1, 1, 2, 34, 35],
    [0, 7],
    [
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4, 4, 4,
        4, 5, 8, 8, 9, 10, 11, 13, 13, 18, 19, 38, 297
    ]
]
var lengths = [2, 5, 10, 20]

describe('scale', () => {
    it('should return goodsize array', () => {
        for(testArray of testArrays) {
            for(lengthValue of lengths) {
                chai.expect(createScale(testArray, lengthValue).length).to.equal(lengthValue);
            }
        }
    });
    it('every element must be bigger than precedent element', () => {
        for(testArray of testArrays) {
            for(lengthValue of lengths) {
                var currentValue = -1;
                var scale = createScale(testArray, lengthValue);
                for (var i = 0; i < lengthValue; i++) {
                    chai.expect(scale[i]>currentValue).to.be.true;
                    currentValue = scale[i];
                }
            }
        }
    });
})