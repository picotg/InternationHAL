<div class="footer" style="background-color:#EEEEEE;padding:20px;margin-top:20px;">
<h2 class="title-is-2">À propos...</h2>
<p class="block">Outil développé en utilisant l'<a target="_blank" href="https://api.archives-ouvertes.fr/docs/search">API de recherche dans HAL</a>.</p>
<p class="block">Code PHP de cet outil disponible <a target="_blank" href="https://gricad-gitlab.univ-grenoble-alpes.fr/picotg/InternationHAL">ici</a> sous licence libre AGPL v3.0.</p>
<p class="block">Il est basé sur InternationHAL de Philippe Gambette disponible <a target="_blank" href="http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php">ici</a>.</p>
</div>