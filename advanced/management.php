<?php

use uga\globhal\query\QueryParameter;

session_start();
require_once dirname(__FILE__, 2).'/vendor/autoload.php';
require_once dirname(__FILE__, 2).'/param.php';

phpCAS::setVerbose(true);
phpCAS::client($parameter['CAS']['version'], $parameter['CAS']['serveur'], $parameter['CAS']['port'], $parameter['CAS']['serveurUri']);
phpCAS::setNoCasServerValidation();
phpCAS::forceAuthentication();
$user = phpCAS::getUser();

$parameter = isset($_SESSION['parameter'])?$_SESSION['parameter']:[];
$parameter = new QueryParameter($parameter);
$noQuery = ($parameter->queryBase==''&&$parameter->customQuery=='');

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <title>Gestion</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body class="content">
<?php

$filename = dirname(__FILE__, 2).'/data/parameter.db';

$PDOOption = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
];

$database = new PDO('sqlite:'.$filename, null, null, $PDOOption);
$query = $database->query('SELECT * FROM user');

if(isset($_POST["del_user"])) {
    $stmt = $database->prepare('DELETE FROM user WHERE id = :id');
    $stmt->execute([
        'id' => $_POST["del_user"]
    ]);
}

if(isset($_POST["new_user"])) {
    $stmt = $database->prepare('INSERT INTO user (login, manager) VALUES (:login, :manager)');
    $stmt->execute([
        'login' => $_POST["new_user"],
        'manager' => isset($_POST["new_user_manager"])?1:0,
    ]);
}

$users = [];
while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
    $users[] = [
        'id' => $row['id'],
        'login' => $row['login'],
        'manager' => $row['manager'],
    ];
}

foreach($users as $row) {
    if($user == $row['login']):
        if(isset($_POST["submit"])) {
            $target = dirname(__FILE__, 2).'/data/halto_db/halto.db';
            if(move_uploaded_file($_FILES["fileDB"]["tmp_name"], $target)) {
                echo 'fichier modifier';
            } else {
                echo 'fichier non modifier '.$_FILES["fileDB"]["error"];
            }
        }
?>
<?php require dirname(__FILE__, 2).'/navbar.php'; ?>
<div class="container">
    <h1 class="title">Gestion</h1>
    <h2 class="subtitle">Base de données Halto</h2>
    <form method="post" enctype="multipart/form-data">
        <p>
            <label for="fileDB">choisir un fichier de base de données :</label>
            <input type="file" name="fileDB" id="fileDB" class="input"><br>
            <input type="submit" value="Envoyer" name="submit" class="input">
        </p>
    </form>
    <h2 class="subtitle">Mise à jour de la base des institution</h2>
    <div class="columns">
        <div class="column">
            <div class="field">
                <label class="checkbox" for="exculudeIncoming">
                    <input type="checkbox" name="exculudeIncoming" id="exculudeIncoming" checked>
                    exclure les structure INCOMING
                </label>
            </div>
            <div class="field">
                <label for="updateOnly">
                    <input type="checkbox" name="updateOnly" id="updateOnly" checked>
                    ne chercher que les structure mise à jours depuit la dernière mise à jours
                </label>
            </div>
            <div class="field">
                <button id="updateStruct" class="button is-primary" type="submit">Mise à jour</button>
            </div>
        </div>
        <div class="column">
            <div class="box">
                nombres de structure trouver : <span id="nbDoc">...</span><br>
                nombres de structure ajouter : <span id="nbAdd">...</span><br>
                nombres de structure mise à jour : <span id="nbUpdate">...</span><br>
            </div>
        </div>
    </div>
    <?php if($row['manager']):?>
        <h2 class="subtitle">Gestion utilisateur</h2>
        <h3>Ajouter utilisateur</h3>
        <form method="post">
            <label class="label" for="new_user">Login à ajouter :</label>
            <input class="input" type="text" name="new_user" id="new_user">
            <label  class="checkbox" for="new_user_manager">
                <input type="checkbox" name="new_user_manager" id="new_user_manager"> manager
            </label>
            <input class="input" type="submit" value="Ajouter">
        </form>
        <h3>Liste des utilisateurs</h3>
        <table class="table">
            <thead>
                <th>Login</th>
                <th>Action</th>
                <th>Manager</th>
            </thead>
            <tbody>
                <?php foreach($users as $row): ?>
                    <tr>
                        <td><?= $row['login'] ?></td>
                        <td>
                            <form method="post"><input type="hidden" name="del_user" value="<?= $row['id'] ?>">
                            <input class="input" type="submit" value="Suppression">
                            </form>
                        </td>
                        <td><?= $row['manager']?'oui':'non' ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>
</body>
<script>
    function updateStruct() {
        $('#updateStruct').unbind();
        $('#updateStruct').removeClass('is-primary');
        $('#updateStruct').addClass('is-warning');
        $('#updateStruct').text('Execution en cours');
        $.ajax({
            url: "../api/updateAllStruct.php",
            data: {
                updateOnly: $('#updateOnly').prop('checked'),
                exculudeIncoming: $('#exculudeIncoming').prop('checked'),
            },
            success: function (data) {
                $('#nbDoc').text(data['nbDoc']);
                $('#nbAdd').text(data['nbDoc']-data['nbUpdate']);
                $('#nbUpdate').text(data['nbUpdate']);
                $('#updateStruct').removeClass('is-warning');
                $('#updateStruct').addClass('is-primary');
                $('#updateStruct').text('Mise à jour');
                $('#updateStruct').click(updateStruct)
            }
        })
    }
    $('#updateStruct').click(updateStruct)
</script>
</html>
<?php
    exit(0);
    endif;
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vous n'avez pas les droits</title>
</head>
<body>
    <h1 class="title">Vous n'avez pas les droits</h1>
</body>
</html>
