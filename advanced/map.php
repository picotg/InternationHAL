<?php
namespace uga\globhal\advanced;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

/**
 * 
 * page de téléchargement des données
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

if(isset($_POST['scale_action'])) {
   if(!isset($_SESSION['colorScales'])||$_POST['scale_action']=='init') {
      $_SESSION['colorScales'] = file_get_contents("data/colorScales.json");
      if($_POST['scale_action']=='init') {
         exit(0);
      }
   }
   if($_POST['scale_action']=='add') {
      $colorScales = json_decode($_SESSION['colorScales']);
      $scaleName = $_POST['scale_name'];
      $scaleValue = $_POST['scale_value'];
      $colorScales->$scaleName = explode(' ', $scaleValue);
      $_SESSION['colorScales'] = json_encode($colorScales);
   } else if($_POST['scale_action']=='del') {
      $colorScales = json_decode($_SESSION['colorScales']);
      $scaleName = $_POST['scale_name'];
      unset($colorScales->$scaleName);
      $_SESSION['colorScales'] = json_encode($colorScales);
   } else if($_POST['scale_action']=='load') {
      $_SESSION['colorScales'] = $_POST['scale_data'];
      exit(0);
   }
} else if(isset($_POST['tilemap_action'])) {
   if(!isset($_SESSION['tilemaps'])||$_POST['tilemap_action']=='init') {
      $_SESSION['tilemaps'] = file_get_contents("data/tilemaps.json");
      if($_POST['tilemap_action']=='init') {
         exit(0);
      }
   }
   if($_POST['tilemap_action']=='add') {
      $tilemaps = json_decode($_SESSION['tilemaps']);
      $tilemapName = $_POST['tilemap_name'];
      $tilemapURL = $_POST['tilemap_url'];
      $tilemapOption = json_decode($_POST['tilemap_option']);
      $tilemaps->$tilemapName = [
         "url" => $tilemapURL,
         "options" => $tilemapOption
      ];
      $_SESSION['tilemaps'] = json_encode($tilemaps);
   } else if($_POST['tilemap_action']=='del') {
      $tilemaps = json_decode($_SESSION['tilemaps']);
      $tilemapName = $_POST['tilemap_name'];
      unset($tilemaps->$tilemapName);
      $_SESSION['tilemaps'] = json_encode($tilemaps);
   } else if($_POST['tilemap_action']=='load') {
      $_SESSION['tilemaps'] = $_POST['tilemap_data'];
      exit(0);
   }
}
$noQuery = !(isset($_SESSION['parameter']) && isset($_SESSION['parameter']['queryBase']))
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
   <link rel="icon" type="type/ico" href="../favicon.ico" />
   <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
   <script src="../js/parametricLink.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
   <title>GlobHal - Avancé - Carte</title>
</head>
<body class="content">
<?php require 'navbar.php'; ?>
<div class="container">
    <h1 class="title">Carte</h1>
    <h2 class="title is-2">Liste des fonds de cartes</h2>
    <h3 class="title is-3">supprimer un fond de cartes</h3>
    <form id="delete_tilemap" method="post">
        <input type="hidden" name="tilemap_action" value="del">
        <p>
            <div class="select">
                <select name="tilemap_name" id="del_tilemap_name"></select>
            </div>
        </p>
        <input type="submit" class="button is-danger" value="supprimer">
    </form>
    <h3 class="title is-3">ajouter un fond de cartes</h3>
    <form method="post">
        <input type="hidden" name="tilemap_action" value="add">
        <p>
            <label class="label" for="tilemap_name">nom</label>
            <input class="input" type="text" name="tilemap_name" id="tilemap_name">
        </p>
        <p>
            <label class="label" for="tilemap_url">url</label>
            <input class="input" type="text" name="tilemap_url" id="tilemap_url">
        </p>
        <p>
            <label class="label" for="tilemap_option">options</label>
            <textarea class="textarea" name="tilemap_option" id="tilemap_option" cols="30" rows="5"></textarea>
        </p>
        <input type="submit" class="button is-primary" value="ajouter">
    </form>

    <h2 class="title is-2">Liste des échelles</h2>
    <h3 class="title is-3">supprimer une échelle</h3>

    <form id="delete_scale" method="POST">
        <input type="hidden" name="scale_action" value="del">
        <div class="select"><select name="scale_name" id="del_scale_name"></select></div>
        <br><br>
        <input type="submit" class="button is-danger" value="supprimer">
    </form><br>

    <h3 class="title is-3">ajouter une échelle</h3>

    <p>Vous pouvez généré des échelle grace à l'outil <a href="https://hihayk.github.io/scale/">scale</a> fait par <a href="https://hayk.design/">Hayk</a>.</p>
    <form method="POST">
        <input type="hidden" name="scale_action" value="add">
        <p>
            <label class="label" for="scale_name">nom de l'échelle</label>
            <input type="text" class="input" name="scale_name" id="scale_name">
        </p>
        <p>
            <label class="label" for="scale_value">échelle</label>
            <input type="text" class="input" name="scale_value" id="scale_value">
        </p>
        <input type="submit" class="button is-primary" value="ajouter">
    </form>

    <h2 class="title is-2">Donnée</h2>

    <h3 class="title is-4">liste d'échelles</h3>

    <a href="../api/colorScales.php">Télécharger la liste des échelles</a><br>
    <a id="uploadColorScalesLink" href="#">Charger une liste d'échelles</a><br>
    <input id="uploadColorScalesFile" type="file" name="loadDataFile" style="display: none;" />
    <a id="init_colorScale" href="#">reinitilisé la liste des échelles</a><br>
    <script>
    $('#init_colorScale').click(ev => {
        ev.preventDefault();
        $.ajax({
            type: 'post',
            data: {scale_action: 'init'},
            success: data => {
                location = location.pathname;
            },
        })
    })
    </script>
    <br>
    <h3 class="title is-3">liste des fonds de cartes</h3>

    <a href="../api/tilemaps.php">Télécharger la liste des fonds de cartes</a><br>
    <a id="uploadTilemapsLink" href="#">Charger une liste d'échelles</a><br>
    <input id="uploadTilemapsFile" type="file" name="loadDataFile" style="display: none;" />
    <a id="init_tilemaps" href="#">reinitilisé la liste des fonds de cartes</a><br>
    <script>
        $('#init_tilemaps').click(ev => {
            ev.preventDefault();
            $.ajax({
                type: 'post',
                data: {tilemap_action: 'init'},
                success: data => {
                    location = location.pathname;
                },
            })
        })
    </script>
</div>
</body>
<script>
   $('#uploadTilemapsLink').click(ev => {
      ev.preventDefault();
      $('#uploadTilemapsFile').trigger('click');
   })
   $("#uploadTilemapsFile:file").change(async function() {
      var tilemapsFile = $('#uploadTilemapsFile').prop('files')[0];
      if(tilemapsFile!=undefined) {
         var tilemaps = await tilemapsFile.text();
         loadDataQuery = $.ajax({
            type:'post',
            data: {
               tilemap_action: 'load',
               tilemap_data: tilemaps
            },
            success: data => {
               location = location.pathname;
            },
         })
      } else {
         $("#dialog").dialog({width: "auto",'title': 'erreur', modal: true});
         await $("#dialog").html('le fichier ne contient pas de donnée.');
      }
   })
   $('#uploadColorScalesLink').click(ev => {
      ev.preventDefault();
      $('#uploadColorScalesFile').trigger('click');
   });
   $("#uploadColorScalesFile:file").change(async function() {
      var colorScalesFile = $('#uploadColorScalesFile').prop('files')[0];
      if(colorScalesFile!=undefined) {
         var colorScales = await colorScalesFile.text();
         loadDataQuery = $.ajax({
            type:'post',
            data: {
               scale_action: 'load',
               scale_data: colorScales
            },
            success: data => {
               location = location.pathname;
            },
         })
      } else {
         $("#dialog").dialog({width: "auto",'title': 'erreur', modal: true});
         await $("#dialog").html('le fichier ne contient pas de donnée.');
      }
   });
   fetch('../api/colorScales.php').then(async data => {
      data = await data.json();
      Object.keys(data).forEach(key => {
         $('#del_scale_name').append($('<option>', {
            value: key,
            text: key
         }));
      })
   })
   fetch('../api/tilemaps.php').then(async data => {
      data = await data.json();
      Object.keys(data).forEach(key => {
         $('#del_tilemap_name').append($('<option>', {
            value: key,
            text: key
         }))
      })
   })
</script>
</html>