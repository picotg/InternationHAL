<?php
namespace uga\globhal\advanced;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

/**
 * 
 * page de téléchargement des données
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
$noQuery = !(isset($_SESSION['parameter']) && isset($_SESSION['parameter']['queryBase']))
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
   <link rel="icon" type="type/ico" href="../favicon.ico" />
   <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
   <script src="../js/parametricLink.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
   <title>GlobHal - Avancé - Export de carte</title>
</head>
<body class="content">

<?php require 'navbar.php'; ?>

<div class="container">

<h1 class="title">Export de carte</h1>

<p>Pour intégré une carte à un site il faut téléchager les fichiers de fonctionnement et le décompresser sur le serveur du site.</p>

<p>il faut ensuite intégré le javascript et le css :</p>

<p>
<code>&lt;script src="js/halmap.js"&gt;&lt;/script&gt;</code><br>
<code>&lt;link rel="stylesheet" href="css/halmap.css"&gt;</code>
</p>

<p>Il faut enfin télécharger les donnée JSON de la carte courante.</p>

<p>Il faut ensuite intégré la carte avec la balise suivante :</p>

<p>
<form>
   <div class="field">
      <label class="label" for="mode">mode</label>
      <div class="select">
         <select name="mode" id="mode">
            <option value="institution">institution</option>
            <option value="domain">domaine</option>
            <option value="crossDomain">pluridisciplinarité</option>
         </select>
      </div>
   </div>
   <div class="field">
      <label class="label" for="colorscale">échelle</label>
      <div class="select">
         <select name="colorscale" id="colorscale">
            <option value="">default</option>
         </select>
      </div>
   </div>
</form>
</p>

<p>
<code id="mapIntegration">&lt;div class="halmap" id="map" style="width:600px; height: 600px;" data-mod="institution" data-source="data/map.json"&gt;&lt;/div&gt;</code>
</p>

<h2 class="subtitle">Fichiers de focntionnement</h2>

<p>Les versions avec jQuery UI et/ou liquidJs sont plus lourde que la version simple, mais, avec un meilleur rendu.</p>

<p>Pour ajouter jQuery UI, il faut ajouter, <b>après le javascript de halmap</b>, le code :</p>

<p>
<code>&lt;script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"&gt;&lt;/script&gt;</code><br>
<code>&lt;link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css"&gt;</code><br>
<code>&lt;script src="js/halmap.jqueryui.js"&gt;&lt;/script&gt;</code>
</p>

<p>Pour ajouter liquidJs, il faut ajouter, <b>après le javascript de halmap</b>, le code :</p>

<p>
<code>&lt;script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"&gt;&lt;/script&gt;</code><br>
<code>&lt;script src="js/halmap.liquid.js"&gt;&lt;/script&gt;</code><br>
<code>
&lt;script&gt;<br>
var engine = new liquidjs.Liquid({ <br>
        root: ['../views/'],<br>
        extname: '.liquid'<br>
    });
&lt;/script&gt;
</code>
</p>

<p><a href="../dl/createzip.php?filename=halmap.zip">télécharger les fichier halmap</a></p>
<p><a href="../dl/createzip.php?filename=halmap.zip&opt=liquid">télécharger les fichier halmap avec liquidJs</a></p>
<p><a href="../dl/createzip.php?filename=halmap.zip&opt=jquery">télécharger les fichier halmap avec jQuery UI</a></p>
<p><a href="../dl/createzip.php?filename=halmap.zip&opt=liquid,jquery">télécharger les fichier halmap avec liquidJs et jQuery UI</a></p>

<h2 class="subtitle">Donnée</h2>

<a id="mapDataDialog" href="#">Télécharger les données JSON de la carte courante</a><br><br>
<div id="dialog" class="content">
</div>
</div>
<?php require 'footer.php'; ?>
</body>
<script>
   var engine = new liquidjs.Liquid({
      root: ['../views/'],
      extname: '.liquid'
   });
   $('#mapDataDialog').click(async ev => {
      ev.preventDefault();
      downloadMapData = await engine.renderFile("dialog/downloadMapData", {});
      $("#dialog").empty();
      await $("#dialog").html(downloadMapData);
      $("#dialog").dialog({
         width: "auto",
         title: 'téléchargement des données',
         position: { my: "center top", at: "center top", of: window }
      });
   })
   function updateMapIntegration() {
      var mapIntegrationStrVal = '<div class="halmap" id="map" style="width:600px; height: 600px;" data-mod="'
      mapIntegrationStrVal += $('#mode').val() + '" data-source="data/map.json"'
      if($('#colorscale').val() != '') {
         mapIntegrationStrVal += " data-colorscale='"+ $('#colorscale').val() +"'"
      }
      mapIntegrationStrVal += '></div>'
      $('#mapIntegration').text(
         mapIntegrationStrVal
      )
   }
   fetch('../api/colorScales.php').then(async data => {
      data = await data.json();
      Object.keys(data).forEach(key => {
         $('#colorscale').append($('<option>', {
            value: JSON.stringify(data[key]),
            text: key
         }));
      })
   })
   $('#mode').change(updateMapIntegration)
   $('#colorscale').change(updateMapIntegration)
</script>
</html>