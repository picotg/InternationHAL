<?php
namespace uga\globhal\advanced;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

/**
 * 
 * page de gestion des données
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

$queryBase = $_SESSION['parameter']['queryBase']??'';
$query=isset($_SESSION['resultRequest']['query'])?$_SESSION['resultRequest']['query']->stringValue:'';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
   <link rel="icon" type="type/ico" href="../favicon.ico" />
   <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
   <script src="../js/parametricLink.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
   <title>GlobHal - Avancé - Données</title>
</head>
<body class="content">

<?php require 'navbar.php'; ?>

<div class="container">
<h1 class="title is-1">Données</h1>
<h2 class="title is-2">Requête en court</h2>

la requête courante est : <a target="_blank" id="showQuery" href="<?= $query?>"><?= $query?></a><br />

<h2 class="title is-2">données brutes</h2>

<?php if ($queryBase!=''): ?>
<a id="cleanSession" href="#">Supprimer les données en cours</a><br>
<script>
   $('#cleanSession').click(ev => {
      ev.preventDefault();
      $.ajax({
         type: "get",
         url: "../index.php",
         data: {destroySession: true},
         success: data => {
            location = location.pathname;
         },
      })
   })
</script>
<?php endif; ?>

<h3 class="title is-3">format fichier</h3>

<?php if ($queryBase!=''): ?>
<a href="../api/rowData.php">télécharger les paramétres de la requête</a> (<a href="../api/rowData.php?pretty">pretty</a>)<br>
<?php if($_SESSION['resultRequest']['rowData']):?>
<a href="../api/rowData.php?data=true">télécharger les paramétres et données de la requête</a> (<a href="../api/rowData.php?data=true&pretty">pretty</a>)<br>
<?php endif; endif; ?>
<a id="uploadDataLink" href="#">Charger un fichier de données</a>
<input id="uploadDataFile" type="file" name="loadDataFile" style="display: none;" />

<h3 class="title is-3">stocage dans le navigateur</h3>

<a id="loadData" href="#">gestion des enregistrements</a>

<div id="dialog" class="content">
</div>
</div>
<?php require 'footer.php'; ?>
</body>
<script>
   var engine = new liquidjs.Liquid({
      root: ['../views/'],
      extname: '.liquid'
   });
   var loadDataQuery;
   $('#loadData').click(async ev => {
      ev.preventDefault();
      loadDataFromBrowser = await engine.renderFile("dialog/loadDataFromBrowser", {
         type: 'data',
         canSave: <?= ($queryBase!='')?"true":"false" ?>
      });
      $("#dialog").html(loadDataFromBrowser);
      $("#dialog").dialog({width: "auto",'title': 'gestion des enregistrement'});
   });
   $("#uploadDataFile:file").change(async function() {
      var dataFile = $('#uploadDataFile').prop('files')[0];
      if(dataFile!=undefined) {
         var filContent = await dataFile.text();
         loadDataQuery = $.ajax({
            type:'post',
            url: '../api/rowData.php',
            data: {data: filContent},
            success: data => {
               location = location.pathname.replace('advanced/advanced.php', 'result/map.php');
            }
         })
         uploadDataWaiting = await engine.renderFile("dialog/uploadDataWaiting", {});
         $("#dialog").html(uploadDataWaiting);
         $("#dialog").dialog({width: "auto",'title': 'envoie en cours', modal: true});
         $('#cancel').click(ev => {
            ev.preventDefault();
            loadDataQuery.abort();
            $("#dialog").dialog('close');
         })
      } else {
         $("#dialog").dialog({width: "auto",'title': 'erreur', modal: true});
         await $("#dialog").html('le fichier ne contient pas de donnée.');
      }
   });
</script>
</html>