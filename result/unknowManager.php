<?php
namespace uga\globhal\result;

use uga\globhal\data\Countries;
use uga\globhal\data\DBStruct;
use uga\globhal\query\UnknownInfos;

/**
 * 
 * Affichage utilisant datatable <https://datatables.net>
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

if(isset($_POST['includeCustomField'])) {
    header('Content-Type: application/json ');
    header('Content-Encoding: UTF-8');
    header('Content-type: application/json; charset=UTF-8');
    header('Content-disposition: attachment;filename=map.json');
    echo "\xEF\xBB\xBF"; // UTF-8 BOM
    $parameter = $_SESSION['parameter'];
    if($_POST['includeCustomField'] == 'true') {
        $_SESSION['parameter']['includeCustomField'] = true;
    } else {
        $_SESSION['parameter']['includeCustomField'] = false;
    }
    
    echo json_encode(['includeCustomField' => $_SESSION['parameter']['includeCustomField']]);
    exit(0);
}

if(!isset($_SESSION['resultRequest'])) {
    echo('Vous devez lancer une recherche <a href="index.php">ici</a> avant d\'ouvrir ce fichier.</body></html>');
    exit(0);
}
$noQuery = false;

$dbStruct = new DBStruct();

$resultRequest = $_SESSION['resultRequest'];
$institutionList = $resultRequest["institutionList"];
 
$unknownInfos = new UnknownInfos();
$findUnknow = $unknownInfos->getUnknownList($institutionList);

if(isset($_POST['countryAction'])) {
    $institution = $_POST['institution'];
    $institution = $unknownInfos->$institution;
    if($_POST['countryAction']=='change') {
        $institution->customCountry = $_POST['country'];
    } else if ($_POST['countryAction']=='del') {
        unset($institution->customCountry);
        exit(0);
    }
}

$countriesArray = [];
foreach(Countries::DATA as $countryCode => $countryName) {
    array_push($countriesArray, [$countryCode, $countryName]);
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>GlobHal - Gestion des institution dont le nom ou le pays est inconnu</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bm/dt-1.12.1/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bm/dt-1.12.1/datatables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <link rel="icon" type="type/ico" href="../favicon.ico" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>
<body class="content">
<?php require 'navbar.php'; ?>
<div class="container">
<div id="dialog" class="content"></div>
<h1 class="title is-1">Gestion des institution dont le nom ou le pays est inconnu</h1>
<table id="unknowTable">
    <thead>
        <tr>
            <th>nom</th>
            <th>id</th>
            <th>dernière vérification</th>
            <th>pays choisi</th>
            <th>validité</th>
        </tr>
    </thead>
    <tbody>
<?php
foreach($findUnknow as $institutionId):
    if(in_array($institutionId, array_keys($institutionList))):
        $data = $unknownInfos->$institutionId;
?>
        <tr>
            <?php if(isset($data->name)): ?>
            <td><?= $data->name ?></td>
            <?php else: ?>
            <td>zz - Inconnue</td>
            <?php endif; ?>
            <td><?= $institutionId ?></td>
            <td><?= date('D M j G:i:s', $dbStruct->getStruct($institutionId)['last_updat']->getTimestamp()) ?></td>
            <?php if(isset($data->country)): ?>
            <td><?= $data->country ?></td>
            <?php
            elseif (isset($data->customCountry)) :
            ?>
            <td><?= $data->customCountry ?> <a class="changeCountry" data-institution="<?= $institutionId ?>" href="#">(changer)</a><br>
            <a class="delCountry" data-institution="<?= $institutionId ?>" href="#">(supprimer)</a>
            </td>
            <?php
            else :
            ?>
            <td>zzz <a class="changeCountry" data-institution="<?= $institutionId ?>" href="#">(changer)</a> </td>
            <?php
            endif;
            ?>
            <td><?= $dbStruct->getStruct($institutionId)['valid'] ?></td>
        </tr>
<?php endif;endforeach; ?>
    </tbody>
</table>
<div class="buttons">
    <button id="useCustomCountry" class="button is-primary">Activer les informations personnalisées</button>
    <button id="dontUseCustomCountry" class="button is-warning">Désactivé les informations personnalisées</button>
</div>
</body>
<script>
   var currentQuery = null;
   var engine = new liquidjs.Liquid({
      root: ['../views/'],
      extname: '.liquid'
   });
    $('#useCustomCountry').click(ev => {
        ev.preventDefault();
        $.ajax({
            data: {
                includeCustomField: true
            },
            type:'post',
            success: async data => {
                location.pathname = location.pathname.replace('unknowManager.php', 'map.php');
            }
        })
    })
    $('#dontUseCustomCountry').click(ev => {
        ev.preventDefault();
        $.ajax({
            data: {
                includeCustomField: false
            },
            type:'post',
            success: async data => {
                location.pathname = location.pathname.replace('unknowManager.php', 'map.php');
            }
        })
    })
    $('#dontUseCustomCountry').click(ev => {
        ev.preventDefault();
    })
    function addChangeCountryEvent() {
        $('.changeCountry').unbind();
        $('.changeCountry').click(async function(ev) {
            ev.preventDefault();
            institutionId = $(this).data('institution');
            data = {
                countries: <?= json_encode($countriesArray) ?>,
                institution: institutionId,
            }
            chouseCountry = await engine.renderFile("unknow/chouseCountry", data);
            $("#dialog").html(chouseCountry);
            $("#dialog").dialog({
                width: "auto",
                title: 'gestion des enregistrement',
                position: { my: "top", at: "top" }
            });
        })
        $('.delCountry').unbind();
        $('.delCountry').click(function(ev) {
            ev.preventDefault();
            institutionId = $(this).data('institution');
            $.ajax({
                type: "post",
                data: {
                    countryAction: 'del',
                    institution: institutionId,
                },
                success: ev => {
                    location = location.pathname;
                }
            })
        })
    }
    addChangeCountryEvent();
    $('#unknowTable').DataTable();
    $('#unknowTable').on('page', ev => {
        addChangeCountryEvent()
    })
</script>
</html>
