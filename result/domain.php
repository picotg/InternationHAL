<?php
namespace uga\globhal\result;

/**
 * 
 * Affichage et option des domaines
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <script src="../js/halmap.js"></script>
    <script src="../js/halmap.liquid.js"></script>
    <script src="../js/halmap.jqueryui.js"></script>
    <script src="../js/parametricLink.js"></script>
    <link rel="icon" type="type/ico" href="../favicon.ico" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/halmap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bm/dt-1.12.1/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bm/dt-1.12.1/datatables.min.js"></script>
   <title>GlobHal - Domaines</title>
</head>
<body class="content">
<?php require_once "navbar.php"; ?>
<div class="container">
    <h1 class="title">Domaines</h1>
    <div id="configPage"></div>
    <div id="changeConfig" class="is-hidden block">
        <button class="button is-primary">Changer les paramètres</button>
    </div>
    <div id="map" data-mod='domain' data-colorscale='["#2A0860","#4C0A7B","#780D96","#AE0FB1","#CB12A9","#D73AD6","#CC63E2","#CB8DEC"]'
    data-source='../api/mapData.php?selectedInstitution=true&selectedDomain=true&selectedCrossDomain=true' data-crossdomainlistid="crossDomainList" data-institutionlistid="institutionList"
    data-domainlistid="domainList" data-tilemapname="default" data-tilemapfile="../api/tilemaps.php"
    data-domainl0="true"></div>
    <div id="table"></div>
</div>
<div id="dialog"></div>
</body>
<script>
    var engine = new liquidjs.Liquid({
        root: ['../views/'],
        extname: '.liquid'
    });
    async function loadPage0() {
        var page0 = await engine.renderFile("domain/page0", {});
        $("#configPage").html(page0);
    }
    loadPage0();
</script>
</html>
