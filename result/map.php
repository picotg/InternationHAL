<?php
namespace uga\globhal\result;

use uga\globhal\query\QueryParameter;

/**
 * 
 * Rendus sous forme de fichier carte glissante utilisant leaflet
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();
$noQuery = FALSE;

if(!isset($_SESSION['resultRequest'])||isset($_SESSION['nbResult'])&&$_SESSION['nbResult']==0) {
    ?>
    <script>
        location.pathname = location.pathname.replace('result/map.php', 'index.php');
    </script>
    <?php
    exit(0);
}

$domain = $_SESSION['parameter']['domain'];
$domainLevel = $_SESSION['parameter']['domainLevel'];

// Get the journal Id in the URL
if(isset($_SESSION['collection'])){
    $collection=$_SESSION['collection'];
} else {
    $collection="";
}

$domains = [];
foreach([0, 1, 2, 3] as $level):
    if(isset($_SESSION['resultRequest']['domainList'][$level])&&$_SESSION['resultRequest']['domainList'][$level]!=[]) {
        array_push($domains, $_SESSION['resultRequest']['domainList'][$level]);
    }
endforeach;

if(isset($_SESSION['parameter'])) {
    $parameter = new QueryParameter($_SESSION['parameter']);
    switch($parameter->queryType) {
        case 'collection':
            $title = "Recherche pour la collection $parameter->queryBase";
            break;
        case 'structure':
            $title = "Recherche pour la structure $parameter->queryBase";
            break;
        case 'halto':
            $title = "Recherche pour ligne halto $parameter->lineAcronym";
            break;
        default:
            $title = "Recherche pour l'auteur $parameter->queryBase";
            break;
    }
    if ($parameter->producedDateInterval) {
       $title .= ' de '.$parameter->producedDateStart.' à '.$parameter->producedDateEnd;
    }
} else {
    $title = "information sur la requête non trouver";
    $domain = "";
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>GlobHAL - Carte</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <script src="../js/halmap.js"></script>
    <script src="../js/halmap.liquid.js"></script>
    <script src="../js/halmap.jqueryui.js"></script>
    <script src="../js/parametricLink.js"></script>
    <link rel="icon" type="type/ico" href="../favicon.ico" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../css/halmap.css">
</head>
<body class="content">

<?php require 'navbar.php'; ?>
<div class="container">
    <h1 class="title is-1"><?= $title ?></h1>
    <div class="columns content is-medium">
        <div id="mapColumn" class="column is-four-fifths">
            <p id="hideMapDiv" class="buttons is-hidden">
                <button id="hideMap" class="button is-link">Afficher les options</button>
            </p>
            <div class="halmap" id="map" style="width:100%; height: 600px" data-mod='institution' data-colorscale='["#2A0860","#4C0A7B","#780D96","#AE0FB1","#CB12A9","#D73AD6","#CC63E2","#CB8DEC"]'
            data-source='../api/mapData.php?selectedInstitution=true&selectedDomain=true&selectedCrossDomain=true' data-crossdomainlistid="crossDomainList" data-institutionlistid="institutionList"
            data-domainlistid="domainList" data-tilemapname="default" data-tilemapfile="../api/tilemaps.php"
            data-domainl0="true"></div>
        </div>
        <div id="crossDomainColumn" style="width:80%;" class="column is-hidden is-four-fifths">
            <p class="buttons">
                <button id="showMap" class="button is-link">Afficher la carte</button>
            </p>
            <div id="crossDomainList" style="overflow:scroll; height: 600px;" class="dataList" data-crossdomaindetails=""></div>
        </div>
        <div class="column">
            <h3 class="title is-3">Mode</h3>
            <p>
                <label class="radio" for="showInstitution">
                    <input type="radio" name="mode" id="showInstitution" checked> institutions
                </label><br>
                <label class="radio" for="showDomain">
                    <input type="radio" name="mode" id="showDomain"> domaines
                </label><br>
                <label class="radio" for="showCrossDomain">
                    <input type="radio" name="mode" id="showCrossDomain"> domaines croisés
                </label>
            </p>
            <h3 id="advancedTitle" class="title is-3">Option avancée</h3>
            <p class="advanced">
                <a class="advanced" id="selectDomain" data-domains='<?= json_encode($domains) ?>' href="#">Restreindre sur un domaine</a>
                <?php if(isset($domain)&&$domain!=''): ?>
                <br>
                le domaine courrant est : <?= $domain ?>
                <?php endif; ?><br>
                <a class="advanced" id="showList" href="#">Afficher la liste complète</a><br>
                <a class="advanced" id="showIgnoreList" href="#">Ignorer des éléments</a><br>
                <a class="advanced" id="loadData" href="#">sauvegarder une requête</a>
            </p>
            <h3 id="displayOptionTitle" class="title is-3">Option d'affichage</h3>
            <p class="displayOption">
                <label class="label displayOption" for="newScale">Changer d'échelle</label>
                <div class="select displayOption"><select name="newScale" id="newScale"></div>
                </select><br>
                <label class="label displayOption" for="tilemapChoice">Choix du fond de carte</label>
                <div class="select displayOption"><select name="tilemapChoice" id="tilemapChoice"></div>
                </select>
            </p>
        </div>
    </div>
    <div id="listDialog" style="display: None;">
        <div id="institutionList" class="dataList" data-institutiondetails=""></div>
        <div id="domainList" class="dataList" data-domaindetails=""></div>
    </div>
    <div id="ignoreListDialog" style="display: None;">
        <div id="institutionIgnoreList" data-institutiondetails=""></div>
        <div id="domainIgnoreList" data-domaindetails=""></div>
    </div>

    <div id="dialog" class="content">
    </div>
</div>
</body>
<script type="text/javascript">
    $('.advanced').toggle();
    $('#advancedTitle').click(function () {
        $('.advanced').toggle();
    });
    $('.displayOption').toggle();
    $('#displayOptionTitle').click(function () {
        $('.displayOption').toggle();
    });
    $('#displayOptionTitle').hover(function() {
        $(this).css('cursor','pointer');
    });
    $('#advancedTitle').hover(function() {
        $(this).css('cursor','pointer');
    });
    var engine = new liquidjs.Liquid({
        root: ['../views/'],
        extname: '.liquid'
    });
    var frenshModeName = {
        domain: 'domaine',
        institution: 'institution',
        crossDomain: 'pluridisciplinarité',
    }
    $('#loadData').click(async ev => {
        ev.preventDefault();
        loadDataFromBrowser = await engine.renderFile("dialog/loadDataFromBrowser", {
            type: 'data',
            canSave: <?= (($parameter->queryBase!=''))?"true":"false" ?>
        });
        $("#dialog").html(loadDataFromBrowser);
        $("#dialog").dialog({width: "auto",'title': 'gestion des enregistrements'});
    });
    $('.dataList').on('changeData', ev => {
        ignoreId = $(ev.target).attr('id').replace('List', 'IgnoreList');
        $(ignoreId).data($(ev.target).data()).trigger('changeData');
    })
    $('#showList').click(ev => {
        ev.preventDefault();
        $('#listDialog').dialog({
            width: "50%",
            title: 'liste des '+ frenshModeName[$('#map').data('mod')] +'s trouvé'+ (($('#map').data('mod')=='institution')?'e':'') +'s',
            position: { my: "center", at: "center", of: window }
        });
    })
    $('#showIgnoreList').click(ev => {
        ev.preventDefault();
        $('#ignoreListDialog').dialog({
            width: "50%",
            title: 'gestion des '+ frenshModeName[$('#map').data('mod')] + 's ignoré'+ (($('#map').data('mod')=='institution')?'e':'') + 's',
            position: { my: "center", at: "center", of: window }
        });
    })
    var modeList = ['domain', 'crossDomain', 'institution'];
    const colorScaleRegEx = new RegExp('^(#[0-F]{6} )+#[0-F]{6}$', 'gm');
    $('#selectDomain').click(async ev => {
        ev.preventDefault();
        domainSelecor = await engine.renderFile("dialog/domainSelector", {
            allDomains: $('#selectDomain').data('domains'),
        });
        $("#dialog").empty();
        await $("#dialog").html(domainSelecor);
        $("#dialog").dialog({
            width: "auto",
            title: 'Choix du domaine',
            position: { my: "center", at: "center", of: window }
        });
    })
    $(async function() {
        <?php foreach([0, 1, 2, 3] as $level):?>
            $('#domainSelector<?= $level ?>').change(ev => {
                currentDomain = $('#domainSelector<?= $level ?> option:selected').val();
                currentDomainLevel = <?= $level ?>;
            })
        <?php endforeach; ?>
        fetch('../api/tilemaps.php').then(async data => {
            data = await data.json();
            $('#map').data('tilemapname', Object.keys(data)[0]).trigger('changeData');
            Object.keys(data).forEach(key => {
                $('#tilemapChoice').append($('<option>', {
                    value: key,
                    text: key
                }));
            })
        })
        $('#tilemapChoice').on('change', function() {
            $('#map').data('tilemapname', this.value).trigger('changeData');
        })
        fetch('../api/colorScales.php').then(async data => {
            data = await data.json();
            Object.keys(data).forEach(key => {
                $('#newScale').append($('<option>', {
                    value: JSON.stringify(data[key]),
                    text: key
                }));
            })
        })
        $('#newScale').on('change', function() {
            newScale = $('#newScale').val();
            $('#map').data('colorscale', JSON.parse(newScale)).trigger('changeData');
        })
    })

    function hideElement(mod) {
        $('#crossDomainColumn').addClass('is-hidden');
        $('#showList').removeClass('is-hidden');
        $('#showIgnoreList').removeClass('is-hidden');
        $('#hideMapDiv').addClass('is-hidden');
        if(mod === 'crossDomain') {
            $('#showList').addClass('is-hidden');
            $('#showIgnoreList').addClass('is-hidden');
            $('#crossDomainColumn').removeClass('is-hidden');
            $('#mapColumn').hide();
            $('#map').data('deactivatehighlight', true);
        } else {
            $('#mapColumn').show();
            $('#map').data('deactivatehighlight', false);
        }
        $('#map').data('mod', mod).trigger('changeData');
    }

    $('#showInstitution').click(ev => {
        hideElement('institution');
    });
    $('#showDomain').click(ev => {
        hideElement('domain');
    });
    $('#showCrossDomain').click(ev => {
        hideElement('crossDomain');
    });
    $('#showMap').click(ev => {
        ev.preventDefault();
        $('#crossDomainColumn').addClass('is-hidden');
        $('#hideMapDiv').removeClass('is-hidden');
        $('#mapColumn').show();
    })
    $('#hideMap').click(ev => {
        ev.preventDefault();
        $('#crossDomainColumn').removeClass('is-hidden');
        $('#hideMapDiv').addClass('is-hidden');
        $('#mapColumn').hide();
    })
    function loadList(mode) {
        return async function() {
            modeList.forEach(otherMode=>{
                $('#'+otherMode+'List').empty();
                $('#'+otherMode+'IgnoreList').empty();
            })
            data = {
                ignoreList: false,
                parameter: $('#map').data('parameter')
            }
            switch (mode) {
                case "collection":
                    data.field = 'collCode_s';
                    break;
                case "structure":
                    data.field = 'structure_t';
                    break;
                case "idhal":
                    data.field = 'authIdHal_i';
                    break;
            }

            data[mode+'List'] = $('#'+mode+'List').data(mode.toLowerCase()+'details').list;
            data[mode+'Infos'] = $('#'+mode+'List').data(mode.toLowerCase()+'details').infos;
            HTMLlist = await engine.renderFile('list/'+mode+'List', data);
            $('#'+mode+'List').html(HTMLlist);
            if(mode!='crossDomain') {
                HTMLIgnorelist = await engine.renderFile('list/'+mode+'IgnoreList', data);
                $('#'+mode+'IgnoreList').html(HTMLIgnorelist);
            }
        }
    }

    modeList.forEach(mode => {
        $('#' + mode + 'List').on('changeData', loadList(mode));
    })
</script>
</html>