<?php
namespace uga\globhal\result;
chdir(dirname(__FILE__, 2));
session_name('globhal');
session_start();

/**
 * 
 * Export au format CSV
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="../js/parametricLink.js"></script>
    <title>GlobHal - Export CSV</title>
</head>
<body class="content">
<?php require 'navbar.php'; ?>
<div class="container">
    <h1 class="title is-1">Export CSV</h1>
    <p><a id="csvlink" class="parameterLink" href="../api/csv.php" id="csv_lik">télécharger fichier csv</a></p>
    <div class="columns">
        <div class="column">
            <h2 class="title is-2">infos par institution</h2>
            <div class="field">
                <label for="countryCode">
                    <input type="checkbox" class="csvlinkParam" name="countryCode" id="countryCode" data-option="countryCode"> codes pays
                </label>
            </div>
            <div class="field">
                <label for="countryName">
                    <input type="checkbox" class="csvlinkParam" name="countryName" id="countryName" data-option="countryName"> noms pays
                </label>
            </div>
            <div class="field">
                <label for="institutionId">
                    <input type="checkbox" class="csvlinkParam" name="institutionId" id="institutionId" data-option="institutionId"> id institutions
                </label>
            </div>
            <div class="field">
                <label for="institutionName">
                    <input type="checkbox" class="csvlinkParam" name="institutionName" id="institutionName" data-option="institutionName"> noms des institutions
                </label>
            </div>
            <div class="field">
                <label for="docType">
                    <input type="checkbox" class="csvlinkParam" name="docType" id="docType" data-option="docType"> types de documents
                </label>
            </div>
            <div class="field">
                <label for="valid">
                    <input type="checkbox" class="csvlinkParam" name="valid" id="valid" data-option="valid"> validité de l'institution dans AureHAL
                </label>
            </div>
            <div class="field">
                <label for="nbPub">
                    <input type="checkbox" class="csvlinkParam" name="nbPub" id="nbPub" data-option="nbPub"> nombre de publications
                </label>
            </div>
            <div class="field">
                <label for="domainList">
                    <input type="checkbox" class="csvlinkParam" name="domainList" id="domainList" data-option="domainList"> liste des domaines racines
                </label>
            </div>
        </div>
        <div class="column">
            <h2 class="title is-2">infos par publication</h2>
            <div class="field">
                <label for="full">
                    <input type="checkbox" class="csvlinkParam" name="full" id="full" data-option="full"> liste par publications
                </label>
            </div>
            <ul style="list-style-type: none;">
                <li>
                    <div class="field">
                        <label for="domainCSV">
                            <input type="checkbox" class="csvlinkParam" name="domainCSV" id="domainCSV" data-option="domainCSV"> domaines
                        </label>
                    </div>
                </li>
                <li>
                    <div class="field">
                        <label for="crossDomainCSV">
                            <input type="checkbox" class="csvlinkParam" name="crossDomainCSV" id="crossDomainCSV" data-option="crossDomainCSV"> pluridisciplinarité
                        </label>
                    </div>
                </li>
                <li>
                    <div class="field">
                        <label for="docid">
                            <input type="checkbox" class="csvlinkParam" name="docid" id="docid" data-option="docid"> docid
                        </label>
                    </div>
                </li>
                <li>
                    <div class="field">
                        <label for="docTitle">
                            <input type="checkbox" class="csvlinkParam" name="docTitle" id="docTitle" data-option="docTitle"> titre des publications
                        </label>
                    </div>
                </li>
                <li>
                    <div class="field">
                        <label for="docAuth">
                            <input type="checkbox" class="csvlinkParam" name="docAuth" id="docAuth" data-option="docAuth"> auteurs
                        </label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
</body>
<script>
    var infoFullId = ['domainCSV', 'crossDomainCSV', 'docid', 'docTitle', 'docAuth']
    $('#full').change(function() {
        for(fullElement of infoFullId) {
            $('#' + fullElement).prop('disabled', !this.checked)
            if(!this.checked) {
                $('#' + fullElement).prop('checked', false).trigger('change');
            }
        }
    })
    $('#full').trigger('change')
</script>
</html>