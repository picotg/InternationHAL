<?php
namespace uga\globhal\result;

use uga\globhal\data\Countries;
use uga\globhal\data\DBStruct;
use uga\globhal\query\UnknownInfos;

/**
 * 
 * Rendu sous forme de tableau HTML basé sur la version classique
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
   <link rel="icon" type="type/ico" href="../favicon.ico" />
   <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
   <title>GlobHal - Liste des institutions</title>
</head>
<body class="content">
<?php
if(isset($_GET['collection'])){
   $collection=$_GET['collection'];
} else {
   $collection="";
}

// verification de la présence du resultat de la requête en session.
if(!isset($_SESSION['resultRequest'])) {
   echo('Vous devez lancer une recherche <a href="index.php">ici</a> avant d\'ouvrir ce fichier.');
   exit(0);
} else {
   $noQuery = false;
   require 'navbar.php'; 
}
?>
<div class="container">
   <h1 class="title is-1">Liste des institutions</h1>
   <div id="institutionDetails"></div>
</div>
<?php

$resultRequest = $_SESSION['resultRequest'];

$newCountryList = (isset($resultRequest["countryList"]))?$resultRequest["countryList"]:[];
$institutionList = (isset($resultRequest["institutionList"]))?$resultRequest["institutionList"]:[];
$countryInstitutions = (isset($resultRequest["countryInstitutions"]))?$resultRequest["countryInstitutions"]:[];
$publicationList = (isset($resultRequest["publicationList"]))?$resultRequest["publicationList"]:[];
$institutionMax = (isset($resultRequest["institutionMax"]))?$resultRequest["institutionMax"]:100;

$dbStruct = new DBStruct();

$countryList = [];
foreach($newCountryList as $country) {
   $countryList[$country] = 0;
   foreach($countryInstitutions[$country] as $institutionId) {
      $countryList[$country] += 1;
   }
}

$parameter = $_SESSION['parameter'];
if($parameter['includeCustomField']) {
   $entryListByInstitution = $resultRequest["entryListByInstitution"];
   $unknownInfos = new UnknownInfos();
   $findUnknow = $unknownInfos->getUnknownList($institutionList);
   foreach($findUnknow as $institutionId) {
      $data = $unknownInfos->$institutionId;
      if (isset($data->customCountry)) {
         if(($key = array_search($institutionId, $countryInstitutions['inc'])) !== false) {
            unset($countryInstitutions['inc'][$key]);
            $countryList['inc']--;
            if($countryList['inc'] <= 0) {
               unset($countryList['inc']);
            }
         }
         $countryInstitutions[$data->customCountry] ??= [];
         if(key_exists($data->customCountry, $countryList)) {
            $countryList[$data->customCountry]++;
         } else {
            $countryList[$data->customCountry] = 1;
         }
         $countryInstitutions[$data->customCountry][] = $institutionId;
      }
   }
}
?>
<div class="container">
<table class="table">
   <thead>
      <tr>
         <th>Pays</th>
         <th>Institutions</th>
      </tr>
   </thead>
   <tbody>
<?php
ksort($countryList);
arsort($countryList);
foreach($countryList as $country => $number){
   if($number>1){$plural="s";}else {$plural="";}
   $countryName = Countries::getCountry($country);
?>
      <tr>
         <th>
            <b><a target="_blank" href="https://hal.archives-ouvertes.fr/search/index/?qa%5BcollCode_s%5D%5B%5D=<?= $collection ?>&qa%5BstructCountry_t%5D%5B%5D=<?= $country ?>&qa%5Btext%5D%5B%5D=&submit_advanced=Rechercher&rows=100"><?= $countryName ?></a></b> (<?= $number ?> institution<?= $plural ?>)
         </th>
         <td>
<?php
   //Build array of institutions of the country associated with number of affiliations for each one:   
   $thisCountryInstitutions=[];
   foreach($countryInstitutions[$country] as $institutionId){
      if(isset($institutionList[$institutionId])) {
         $thisCountryInstitutions[$institutionId] = intval($institutionList[$institutionId]);
      }
   }
   arsort($thisCountryInstitutions);
   $instNb = count($thisCountryInstitutions);
   $instIt = 0;
   foreach($thisCountryInstitutions as $institutionId => $nb){
      $plural = ($nb>1)?'s':'';
      $fontSize=10+intval(1+9.99999*(log($nb))/log($institutionMax));
      $clen_name = str_replace("'", "%%SINGLE_QUOTE%%", $dbStruct->getStruct(intval($institutionId))['name']);
?>
      <a class="institutionLink" data-institution="<?= $institutionId ?>" data-country="<?= $country ?>" target="_blank" style="color:rgb(<?= rand(0,200) ?>, <?= rand(0,200) ?>, <?= rand(0,200) ?>);font-size:"<?= $fontSize ?>px" title="Institution correspondant à <?= $institutionList[$institutionId]?> affiliation<?= $plural ?>"
      href="https://hal.archives-ouvertes.fr/search/index/?qa%5BcollCode_s%5D%5B%5D=<?= $collection ?>&qa%5BstructId_i%5D%5B%5D=<?= $institutionId ?>&qa%5Btext%5D%5B%5D=&submit_advanced=Rechercher&rows=100"><?= $dbStruct->getStruct(intval($institutionId))['name']?></a><?= (++$instIt!==$instNb)?' - ':'' ?>
<?php
   }
   echo "</td>";
}?>
   </tbody>
</table>
</div>
</body>
<script>
   var engine = new liquidjs.Liquid({
      root: ['../views/'],
      extname: '.liquid'
   });
   $('.institutionLink').click(async function(ev) {
      ev.preventDefault();
      location.href = "#";
      $.ajax({
         url: '../api/institutionData.php',
         data: {
            institutionId: $(ev.target).data('institution'),
            country: $(ev.target).data('country'),
         },
         success: async data => {
            data.queryType = "<?= $_SESSION['parameter']['queryType'] ?>"
            data.queryBase = "<?= $_SESSION['parameter']['queryBase'] ?>"
            institutionDetails = await engine.renderFile("details/institution", data);
            $("#institutionDetails").html(institutionDetails);
         }
      })
   })
</script>
</html>