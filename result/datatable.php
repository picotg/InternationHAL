<?php
namespace uga\globhal\result;

use uga\globhal\data\Countries;
use uga\globhal\data\DBStruct;
use uga\globhal\query\UnknownInfos;
use uga\hallib\QueryIterator;
use uga\hallib\ref\domain\DomainSelector;

/**
 * 
 * Affichage utilisant datatable <https://datatables.net>
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

$docurl = isset($_GET['docurl'])?true:false;
$noQuery = false;

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bm/dt-1.12.1/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bm/dt-1.12.1/datatables.min.js"></script>
    <link rel="icon" type="type/ico" href="../favicon.ico" />
    <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
    <title>GlobHal - datatable</title>
</head>
<body class="content">
<?php

$domainSelector = new DomainSelector();
$domainSelector->extractData();

if(!isset($_SESSION['resultRequest'])) {
   echo('Vous devez lancer une recherche <a href="index.php">ici</a> avant d\'ouvrir ce fichier.</body></html>');
   exit(0);
}

if($docurl) {
    $query = $_SESSION['resultRequest']['query'];
    $query->emptyReturnedFields();
    $query->addReturnedFields(['docid', 'halId_s']);
    $requestIterator = new QueryIterator($query);
    $docurls = [];
    foreach($requestIterator as $doculrResult) {
        $docurls[$doculrResult->docid] = 'https://hal.archives-ouvertes.fr/'.$doculrResult->halId_s;
    }
}

$resultRequest = $_SESSION['resultRequest'];

$newCountryList = $resultRequest["countryList"];
$institutionList = $resultRequest["institutionList"];
$countryInstitutions = $resultRequest["countryInstitutions"];
$institutionMax = $resultRequest["institutionMax"];
$entryListByInstitution = $resultRequest["entryListByInstitution"];
$institutionDocTypeList = $resultRequest["institutionDocTypeList"];
$docTypeList = $resultRequest["docTypeList"];
$institutionCrossDomains = $resultRequest["institutionCrossDomains"];
$crossDomainsList = $resultRequest["crossDomainsList"];

$dbStruct = new DBStruct();

$countryList = [];

foreach($newCountryList as $country) {
    $countryList[$country] = 0;
    foreach($countryInstitutions[$country] as $institutionId) {
        $countryList[$country] += 1;
    }
}

$parameter = $_SESSION['parameter'];
if($parameter['includeCustomField']) {
   $entryListByInstitution = $resultRequest["entryListByInstitution"];
   $unknownInfos = new UnknownInfos();
   $findUnknow = $unknownInfos->getUnknownList($institutionList);
   foreach($findUnknow as $institutionId) {
      $data = $unknownInfos->$institutionId;
      if (isset($data->customCountry)) {
         if(($key = array_search($institutionId, $countryInstitutions['inc'])) !== false) {
            unset($countryInstitutions['inc'][$key]);
            $countryList['inc']--;
         }
         $countryInstitutions[$data->customCountry] ??= [];
         if(!in_array($data->customCountry, $countryList)) {
            $countryList[$data->customCountry] = 0;
         } else {
            $countryList[$data->customCountry]++;
         }
         $countryInstitutions[$data->customCountry][] = $institutionId;
      }
   }
}
?>

<?php require 'navbar.php'; ?>
<div class="container">

<table id="dataTableQuery" class="display" style="width:100%">
    <thead>
        <tr>
            <th>code pays</th>
            <th>nom pays</th>
            <th>id institution</th>
            <th>nom institution</th>
            <th>docid</th>
            <th>docType</th>
            <th>domain</th>
            <th>validité institution</th>
        </tr>
    </thead>
    <tbody>
<?php
foreach($countryList as $country => $number) {
    foreach($countryInstitutions[$country] as $institutionId) {
        $data = $dbStruct->getStruct($institutionId);
        if($data!=null and $data['name']!=null):
            $currentIntitution = $data['name'];
            foreach($entryListByInstitution[$institutionId] as $entry) {
                $docid = isset($entry->docid)?$entry->docid:'';
                $docType_s = isset($entry->docType_s)?$entry->docType_s:'';
                $countryName = Countries::getCountry($country);
                $countryUrl = "https://hal.archives-ouvertes.fr/search/index/?qa%5BcollCode_s%5D%5B%5D=&qa%5BstructCountry_t%5D%5B%5D=$country&qa%5Btext%5D%5B%5D=&submit_advanced=Rechercher&rows=100";
                $instUrl = "https://hal.archives-ouvertes.fr/search/index/?qa%5BcollCode_s%5D%5B%5D=&qa%5BstructId_i%5D%5B%5D=$institutionId&qa%5Btext%5D%5B%5D=&submit_advanced=Rechercher&rows=100"
?>
<tr>
    <td><a target="_blank" href="<?= $countryUrl ?>"><?= ($country!='inc')?$country:'zzz' ?></a></td>
    <td><a target="_blank" href="<?= $countryUrl ?>"><?= $countryName ?></a></td>
    <td><a target="_blank" href="<?= $instUrl ?>"><?= $institutionId ?></a></td>
    <td><?= $currentIntitution ?></td>
<?php if($docurl): ?>
    <td><a target="_blank" href="<?= $docurls[$docid] ?>"><?= $docid ?></a></td>
<?php else:?>
    <td><?= $docid ?></td>
<?php endif; ?>
    <td><?= $docType_s ?></td>
    <td>
<?php
            if(isset($entry->domainAllCode_s)&&is_array($entry->domainAllCode_s)) {
                foreach($entry->domainAllCode_s as $i => $code_s) {
                    $domainDoc = $domainSelector->code2doc($code_s);
                    $domainName = isset($domainDoc->fr_domain_s)?DomainSelector::cleanDomainName($domainDoc->fr_domain_s):'';
                    if($i!=0) echo ', ';
                    echo $domainName;
                }
            }
?>
    </td>
    <td><?= $data['valid'] ?></td>
</tr>
<?php
            }
        endif;
    }
}
?>
</tr>
    </tbody>
</table>
<br><br>
<a href="datatable.php?docurl=true">recharger avec les url pour les document</a><br>
<a id="loadData" href="#">gestion des requête enregistrements</a>
</div>
<div id="dialog" class="content">
</div>
</body>
<script>
    var engine = new liquidjs.Liquid({
        root: ['../views/'],
        extname: '.liquid'
    });
    $(document).ready(function () {
        $('#dataTableQuery').DataTable();
    });
    $('#loadData').click(async ev => {
        ev.preventDefault();
        loadDataFromBrowser = await engine.renderFile("dialog/loadDataFromBrowser", {
            type: 'data',
            canSave: <?= ($_SESSION['parameter']['queryBase']!='')?"true":"false" ?>
        });
        $("#dialog").html(loadDataFromBrowser);
        $("#dialog").dialog({width: "auto",'title': 'gestion des enregistrement'});
    });
</script>
</html>