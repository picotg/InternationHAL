<?php
namespace uga\globhal;
require_once 'vendor/autoload.php';
$URI = $_SERVER['REQUEST_URI'];  
$isIndex = substr($URI, -strlen('index.php'))==='index.php' || substr($URI, -1)==='/';
$isResult = str_contains($URI, 'result');
$isAdvanced = str_contains($URI, 'advanced');
$noQuery = !(isset($_SESSION['parameter']) && isset($_SESSION['parameter']['queryBase']));
?>
<nav class="navbar" role="navigation" aria-label="main navigation">
   <div class="navbar-brand">
      <a class="navbar-item" href="index.php">
         <img src="<?= !$isIndex?'../':'' ?>img/logo_uga.png" height="28">
      </a>
   </div>
   <div class="navbar-menu">
      <div class="navbar-start">
         <a class="navbar-item" href="<?= !$isIndex?'../':'' ?>index.php"  <?= (!$isAdvanced && !$isResult)?'style="font-weight: bold;"':''?>>
            Requête
         </a>
         <?php if (!$noQuery): ?>
            <a href="<?= !$isIndex?'../':'' ?>result/map.php" class="navbar-item"  <?= ($isResult)?'style="font-weight: bold;"':''?>>
               Résutats
            </a>
         <?php else: ?>
            <a href="#" class="navbar-item warningLink">
               Résutats
            </a>
            <script>
               $('.warningLink').click(ev => {
                  ev.preventDefault();
                  alert('Vous devez effectuer une requête avant de pouvoir accéder à ce lien.');
               });
            </script>
         <?php endif; ?>
         <a href="<?= !$isIndex?'../':'' ?>advanced/data.php" class="navbar-item"   <?= ($isAdvanced)?'style="font-weight: bold;"':''?>>
            Avancée
         </a>
      </div>
   </div>
</nav>
<?php if (!$noQuery && $isResult): ?>
<div class="tabs is-boxed is-small" role="navigation" style="z-index: 0;">
   <ul>
      <li <?= str_ends_with($URI, 'map.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>result/map.php">
            Carte
         </a>
      </li>
      <li <?= str_ends_with($URI, 'domain.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>result/domain.php">
            Domaines
         </a>
      </li>
      <li <?= str_ends_with($URI, 'classique.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>result/classique.php">
            Liste institutions
         </a>
      </li>
      <li <?= str_ends_with($URI, 'datatable.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>result/datatable.php">
            Datatable
         </a>
      </li>
      <li <?= str_ends_with($URI, 'unknowManager.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>result/unknowManager.php">
            Infos inconnues
         </a>
      </li>
      <li <?= str_ends_with($URI, 'csv.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>result/csv.php">
            Expor CSV
         </a>
      </li>
   </ul>
</div>
<?php endif; ?>

<?php if ($isAdvanced): ?>
<div class="tabs is-boxed is-small" role="navigation" style="z-index: 0;">
   <ul>
      <li <?= str_ends_with($URI, 'data.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>advanced/data.php">
            Données
         </a>
      </li>
      <li <?= (str_ends_with($URI, 'map.php') && !str_ends_with($URI, 'export_map.php'))?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>advanced/map.php">
            Carte
         </a>
      </li>
      <li <?= str_ends_with($URI, 'export_map.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>advanced/export_map.php">
            Exporte Carte
         </a>
      </li>
      <li <?= str_ends_with($URI, 'management.php')?'class="is-active"':'' ?>>
         <a href="<?= !$isIndex?'../':'' ?>advanced/management.php">
            Administation
         </a>
      </li>
   </ul>
</div>
<?php endif; ?>
