<?php

namespace uga\globhal\test\hallib;

use DateTime;
use PHPUnit\Framework\TestCase;
use uga\globhal\data\DBStruct;

/**
 * 
 * Test accés date last update
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

class LastUpdateTest extends TestCase {
    public function testGetLastUpdate() {
        $db = new DBStruct(dirname(__FILE__).'/struct.test.db');
        $this->assertEquals(DateTime::createFromFormat('Y-m-d H:i:s', '2023-11-13 13:44:00'), $db->getLastUpdate());
    }
}
