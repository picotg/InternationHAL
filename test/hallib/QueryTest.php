<?php

namespace uga\globhal\test\hallib;

use PHPUnit\Framework\TestCase;
use uga\globhal\query\QueryParameter;
use uga\globhal\query\QueryResult;

/**
 * 
 * Search Hal pour les publication sans DOI.
 * 
 * @author Gaël PICOT
 * 
 * iDOIne :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

class QueryTest extends TestCase {
    private string $idHalURL_API = 'https://api.archives-ouvertes.fr/search/?q=authIdHal_i:830244&fl=docid,instStructCountry_s&rows=1000';

    public function testidHalQuery() {
        $parameter = new QueryParameter([
            'queryBase' => '830244',
            'queryType' => 'idhal',
            'instance' => 'saga',
        ]);
        $qr = new QueryResult($parameter);
        $qr->extractAllData();
        $data = $qr->dataArray;
        $countryList = $data["countryList"];
        $countryInstitutions = $data["countryInstitutions"];
        $entryListByInstitution = $data["entryListByInstitution"];
        $directData = json_decode(file_get_contents($this->idHalURL_API));
        $directId = [];
        $directCountry = [];
        foreach($directData->response->docs as $doc) {
            array_push($directId, $doc->docid);
            $directCountry[$doc->docid] = $doc;
        }
        foreach($countryList as $country) {
            foreach($countryInstitutions[$country] as $institutionId) {
                foreach($entryListByInstitution[$institutionId] as $entry) {
                    $this->assertContains($entry->docid, $directId);
                    $this->assertContains($country, $directCountry[$entry->docid]->instStructCountry_s);
                }
            }
        }
    }
}
