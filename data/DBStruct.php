<?php
namespace uga\globhal\data;

use DateTime;
use PDO;
use stdClass;
use uga\hallib\OneDocQuery;
use uga\hallib\queryDefinition\IntervalElement;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\queryDefinition\QueryTreeElement;
use uga\hallib\queryDefinition\Result;
use uga\hallib\QueryIterator;
use uga\hallib\ref\structure\StructureField;
use uga\hallib\ref\structure\StructureQuery;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * Génére les donnée de la carte pour la session courante.
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


 class DBStruct {
    private PDO $database;
    private static array $cacheInfos = [];
    private array $tablesSQL = [
        'CREATE TABLE "structure" (
            "id"	            INTEGER UNIQUE,
            "docid"	            INTEGER NOT NULL UNIQUE,
            "name"	            TEXT,
            "country"	        TEXT,
            "valid"	            TEXT,
            "last_updat"        TEXT,
            PRIMARY KEY("id" AUTOINCREMENT)
        );',
    ];

    public function insertDoc(Result $doc): array {
        $selectStmt = $this->database->prepare('SELECT * FROM structure WHERE docid = :docid');
        if(isset($doc->country_s)) {
            $country = $doc->country_s;
        } else {
            $country = null;
        }
        if(isset($doc->name_s)) {
            $name = $doc->name_s;
        } else {
            $name = null;
        }
        $sqlDoc = [
            'docid' => $doc->docid,
            'name' => $name,
            'country' => $country,
            'valid' => $doc->valid_s,
            'last_updat' => date('Y-m-d H:i:s'),
        ];
        $selectStmt->execute(['docid' => $doc->docid]);
        if(!$selectStmt->fetch()) {
            $insertStmt = $this->database->prepare('INSERT INTO structure (docid, name, country, valid, last_updat) VALUES (:docid, :name, :country, :valid, :last_updat)');
            $insertStmt->execute($sqlDoc);
            $sqlDoc['update'] = false;
        }
        else {
            $updateStmt = $this->database->prepare('UPDATE structure SET name = :name, country = :country, valid = :valid, last_updat = :last_updat WHERE docid = :docid');
            $updateStmt->execute($sqlDoc);
            $sqlDoc['update'] = true;
        }
        return $sqlDoc;
    }

    public function __construct(string $dbFile='') {
        $dataBaseFilename = ($dbFile=='')?(dirname(__FILE__).'/struct.db'):$dbFile;
        $dbExist = !file_exists($dataBaseFilename);
        $this->database = new PDO('sqlite:'.$dataBaseFilename);
        if($dbExist) {
            foreach($this->tablesSQL as $createDBSQL) {
                $this->database->exec($createDBSQL);
            }
        }
        $this->database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    }

    public function getStruct(int $docid): ?array {
        if(!key_exists($docid, static::$cacheInfos)) {
            static::$cacheInfos[$docid] = $this->_getStruct($docid);
        }
        return static::$cacheInfos[$docid];
    }

    public function _getStruct(int $docid): ?array {
        $selectStmt = $this->database->prepare('SELECT * FROM structure WHERE docid = :docid');
        $selectStmt->execute(['docid'=>$docid]);
        if($doc = $selectStmt->fetch()) {
            return [
                'docid' => $doc['docid'],
                'name' => $doc['name'],
                'country' => $doc['country'],
                'valid' => $doc['valid'],
                'last_updat' => DateTime::createFromFormat('Y-m-d H:i:s', $doc['last_updat']),
            ];
        } else {
            $getQuery = new StructureQuery();
            $getQuery->baseQuery = new LiteralElement([
                "value" => $docid,
                "field" => StructureField::getVarient('docid')
            ]);
            $getQuery->addFilter(new LiteralElement([
                "value" => "institution",
                "field" => StructureField::getVarient('type_s')
            ]));
            $getQuery->addReturnedField(StructureField::getVarient('country_s'));
            $getQuery->addReturnedField(StructureField::getVarient('docid'));
            $getQuery->addReturnedField(StructureField::getVarient('name_s'));
            $getQuery->addReturnedField(StructureField::getVarient('valid_s'));
            $getDoc = new OneDocQuery($getQuery);
            $doc = $getDoc->getReult();
            if($doc==null) {
                return null;
            }
            return $this->insertDoc($doc);
        }
    }

    public function getLastUpdate(): DateTime {
        $lastUpdateStmt = $this->database->prepare('SELECT last_updat FROM structure GROUP BY last_updat');
        $lastUpdateStmt->execute();
        $lastDate = DateTime::createFromFormat('Y-m-d', '1800-01-01');
        while($row = $lastUpdateStmt->fetch(PDO::FETCH_ASSOC)) {
            $newdate = DateTime::createFromFormat('Y-m-d H:i:s', $row['last_updat']);
            $lastDate = ($lastDate>$newdate)?$lastDate:$newdate;
        }
        return $lastDate;
    }

    public function updateAllStruct(bool $excludeIncoming=true, bool $updateOnly=true): array {
        $updateQuery = new StructureQuery();
        $updateQuery->baseQuery = new LiteralElement([
            "value" => "institution",
            "field" => StructureField::getVarient('type_s')
        ]);
        if($excludeIncoming) {
            $updateQuery->addFilter(new QueryTreeElement([
                "elements" => ["VALID", "OLD"],
                "field" => StructureField::getVarient('valid_s')
            ]));
        }
        if($updateOnly) {
            $dateFilter = new IntervalElement([
                'field' => StructureField::getVarient('updateDate_tdate'),
                'convertorName' => 'DateTime'
            ]);
            $dateFilter->minValue = $this->getLastUpdate();
            $updateQuery->addFilterQuery($dateFilter);
        }
        $updateQuery->rows = 10000;
        $updateQuery->useCursor = true;
        $updateQuery->addReturnedField(StructureField::getVarient('country_s'));
        $updateQuery->addReturnedField(StructureField::getVarient('docid'));
        $updateQuery->addReturnedField(StructureField::getVarient('name_s'));
        $updateQuery->addReturnedField(StructureField::getVarient('valid_s'));

        $qi = new QueryIterator($updateQuery);
        
        $nbDoc = 0;
        $nbUpdate = 0;

        foreach($qi as $doc) {
            $nbDoc++;
            if($this->insertDoc($doc)['update']) $nbUpdate++;
        }
        return [
            'nbDoc' => $nbDoc,
            'nbUpdate' => $nbUpdate
        ];
    }
 }
