<?php
/**
 * 
 * Générateur d'archive à télécharger pour l'intégration de carte.
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
namespace uga\globhal\dl;
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\queryDefinition\DataContainer;

class MapExporter extends DataContainer {
    protected array $options = [];
    protected array $files = [
        'css/halmap.css',
        'js/halmap.js',
    ];
    protected string $filesPath;
    protected array $modes = ['crossDomain', 'domain', 'institution'];

    public function getFilename(): string {
        return (count($this->options)!==0)?'halmap_'.implode('_', $this->options).'.zip':'halmap.zip';
    }

    public function __construct($values=null) {
        parent::__construct($values);
        $this->filesPath = dirname(__FILE__, 2);
    }

    public function setOptions($options) {
        if(is_string($options)) {
            $options = explode(',', $options);
        }
        $realOption = [];
        foreach($options as $option) {
            if(method_exists($this, 'add'.ucfirst($option))) {
                array_push($realOption, $option);
            }
        }
        $this->options = $realOption;
    }

    public function addTilemaps() {
        array_push($this->files, 'data/tilemaps.json');
    }

    public function addJquery() {
        array_push($this->files, 'js/halmap.jqueryui.js');
    }

    public function addLiquid() {
        foreach($this->modes as $mode) {
            array_push($this->files, 'views/popup/popup'.ucfirst($mode).'.liquid');
            array_push($this->files, 'views/info/info'.ucfirst($mode).'.liquid');
            array_push($this->files, 'views/list/'.$mode.'List.liquid');
            array_push($this->files, 'views/list/'.$mode.'IgnoreList.liquid');
        }
        array_push($this->files, 'views/info/infoDefault.liquid');
        array_push($this->files, 'js/halmap.liquid.js');
    }

    public function createZip() {
        foreach($this->options as $option) {
            $methodName = 'add'.ucfirst($option);
            if(method_exists($this, $methodName)) {
                $this->$methodName();
            }
        }
        $zip = new \ZipArchive();
        $archiveFilename = $this->filename;
        if (!$zip->open($archiveFilename, \ZIPARCHIVE::CREATE )) {
            exit("cannot open $archiveFilename");
        }
        foreach($this->files as $file) {
            $zip->addFile($this->filesPath.'/'.$file, $file);
        }
        $zip->close();
    }
}
