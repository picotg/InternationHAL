<?php
namespace uga\globhal;

use uga\globhal\query\QueryParameter;
use uga\globhal\query\QueryResult;
use uga\hallib\halto\HaltoDB;
use uga\hallib\ref\domain\DomainSelector;
use uga\hallib\HTMLGenerator\SelectorGenerator;

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

/**
 * 
 * page d'accueil
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


$haltoDBFile = dirname(__FILE__).'/data/halto_db/halto.db';
if(file_exists($haltoDBFile)) {
   $haltoDB = new HaltoDB();
   $haltoDB->setDatabase($haltoDBFile);
}

if(($_GET['destroySession']??'false')==='true') {
   session_destroy();
}
$parameter = new QueryParameter($_SESSION['parameter']??[]);
$noQuery = ($parameter->queryBase==''&&$parameter->customQuery=='');

// si la demande est une requête
if(isset($_POST['queryBase'])||isset($_POST['customQuery'])) {
   header('Content-Type: application/json ');
   header('Content-Encoding: UTF-8');
   $sessionParameter = $parameter;
   $parameter = new QueryParameter($_POST);

   $parameter = $parameter;
   $_SESSION['parameter'] = $parameter->toArray();
   $_SESSION['crossDomainIgnorList'] = [];
   $qr = new QueryResult($parameter);

   $qr->extractAllData();

   // enregistrer les résultats de la requête dans la session.
   $_SESSION['resultRequest'] = $qr->dataArray;
   $_SESSION['parameter'] = $parameter->toArray();
   $_SESSION['nbResult'] = $qr->getNbResult();
   $_SESSION['query'] = $qr->query;

   echo json_encode($parameter->toArray());
   exit(0);
}

// si la demande est l'affichage des résultats
$query = $_SESSION['resultRequest']['query']??'';

$domainSelector = new DomainSelector();
$domainSelector->extractData();
$domainSelector->level = 0;
$selectGenerator = new SelectorGenerator($domainSelector);
$selectGenerator->name = 'domainL0';
$selectGenerator->label = 'Domaine';
$selectGenerator->frameWorksName = 'bulma';
$selectGenerator->addField('', 'tous les domaines');

$domains = [];
foreach([0, 1, 2, 3] as $level):
   if(($_SESSION['resultRequest']['domainList'][$level]??[])!=[]) {
      array_push($domains, $_SESSION['resultRequest']['domainList'][$level]);
   }
endforeach;

if ($parameter->queryBase!='') {
   switch($parameter->queryType) {
      case 'collection':
         $title = "Recherche pour la collection $parameter->queryBase";
         break;
      case 'structure':
         $title = "Recherche pour la structure $parameter->queryBase";
         break;
      case 'halto':
         $title = "Recherche pour ligne halto $parameter->lineAcronym";
         break;
      default:
         $title = "Recherche pour l'auteur $parameter->queryBase";
         break;
   }
   if ($parameter->producedDateInterval) {
      $title .= ' de '.$parameter->producedDateStart.' à '.$parameter->producedDateEnd;
   }
} else {
   $title = 'Analyse des collaborations internationales utilisant Hal';
}
?>
<!DOCTYPE html>
<html>

<head>
   <title>GlobHAL - Requête</title>
   <meta name="Description" content="<?= $title ?>">  
   <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <link rel="icon" type="type/ico" href="favicon.ico" />
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
   <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="https://code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
   <script src="https://cdn.jsdelivr.net/npm/liquidjs/dist/liquid.browser.min.js"></script>
</head>
<body class="content">
<?php require 'navbar.php'; ?>
<div class="container">
<h1 class="title is-1"><?= $title ?></h1>
<p>
<form id="queryForm" method="get">
<p>
   <div class="field">
      <label class="label" for="instance">portail</label>
      <div class="select">
         <select name="instance" id="instance">
            <option value="">Aucun</option>
         </select>
      </div>
   </div>
   <div class="field">
      <label class="label" for="queryType">Type de echerche</label>
      <div class="select">
         <select name="queryType" id="queryType">
            <option value="collection">collection</option>
            <option value="structure"<?= ($parameter->queryType=='structure')?' selected':'' ?>>structure</option>
            <option value="idhal"<?= ($parameter->queryType=='idhal')?' selected':'' ?>>personne</option>
            <?php $haltoSelected = ($parameter->queryType=='halto')?' selected':''; ?>
            <?= isset($haltoDB)?'<option value="halto"'.$haltoSelected.'>Halto</option>':'' ?>
         </select>
      </div>
   </div>
   <?php if(isset($haltoDB)): ?>
   <div id="haltoSelector" class="field">
      <label class="label" for="line">ligne Halto</label>
      <div class="select">
         <select name="line" id="line">
            <?php foreach($haltoDB->lines as $line): ?>
               <option value="<?= $line['id'] ?>"<?= ($parameter->queryType=='halto' && $line['id']==$parameter->queryBase)?' selected':''?>><?= $line['label'] ?></option>
            <?php endforeach; ?>
         </select>
      </div>
   </div>
   <?php endif; ?>
   <div id="queryBaseInput" class="field">
      <label class="label" for="queryBase">Recherche</label>
      <input type="text" class="input" id="queryBase" name="queryBase" value="<?=$parameter->queryBase?>"><a class="label" href="#" id="advencedSearch">(Recherche avancée)</a>
   </div>
</p>
<p>
   <fieldset class="control box">
      <legend class="label has-text-centered">années</legend>
      <div class="field"><input type="radio" name="producedDateInterval" id="producedDateIntervalAll"<?= (!$parameter->producedDateInterval)?' checked':''?>><label for="producedDateIntervalAll"> toutes</label></div>
      <div class="field"><input type="radio" name="producedDateInterval" id="producedDateInterval"<?= ($parameter->producedDateInterval)?' checked':''?>><label for="producedDateInterval"> de <input type="number" class="input" style="width: 20%;" name="producedDateStart" id="producedDateStart" value="<?= $parameter->producedDateStart ?>"> à <input type="number" class="input" style="width: 20%;" name="producedDateEnd" id="producedDateEnd" value="<?= $parameter->producedDateEnd ?>"></label></label></div>
   </fieldset>
</p>
<div class="field">
   <label class="label" for="docTypes">type de document</label>
   <div class="select is-multiple">
      <select name="docTypes" id="docTypes" multiple>
         <option value="" selected>tous</option>
      </select>
   </div>
</div>
<a id="advencedSearchParameterLink" href="#">Paramètres avancé</a>
<fieldset class="control box" id="advencedSearchParameter" style="display: none;">
   <div class="field">
      <label class="checkbox" for="noIncoming">
         <input type="checkbox" name="noIncoming" id="noIncoming"<?= $parameter->noIncoming?' checked':''?>>
         exclure les structure ICOMING
      </label>
   </div>
   <div class="field">
      <label class="label" for="customQuery">requête personnalisé</label>
      <input class="input" type="text" name="customQuery" id="customQuery" value="<?= $parameter->customQuery ?>">
   </div>
   <div class="field">
      <label class="label" for="complement">compléter la requête envoyée à l'API HAL</label>
      <input type="text" class="input" style="width: 40%;" name="complement" id="complement" value="<?=$parameter->complement?>">
   </div>
   <div class="field">
      <?= $selectGenerator->generateSelectField() ?>
      <ul id="subDomainList"></ul>
   </div>
   <div class="field">
      <label class="checkbox" for="france">
         <input type="checkbox" name="france" id="france"<?= $parameter->france?' checked':''?>>
       inclure la France dans les résultats (il est possible que des institutions étrangères soient indiquées en France par erreur) ?</label>
   </div>
   <div class="field">
      <label class="checkbox" for="includeCustomField">
         <input type="checkbox" name="includeCustomField" id="includeCustomField"<?= $parameter->includeCustomField?' checked':''?>>
         utilisé les valeurs personnalisé pour les pays inconnu
      </label>
   </div>
   <div class="field">
      <label class="checkbox">
         <input type="checkbox" name="maxResult" id="maxResult"<?= $parameter->maxResult?' checked':''?>> limité les résultat à une page
      </label>
   </div>
   <div class="field">
      <label class="checkbox">
         <input type="checkbox" name="saveData" id="saveData"<?= $parameter->saveData?' checked':'' ?>> enregistrer les données brutes
      </label>
   </div>
   <div class="field">
      <label class="label" for="rows">nombre de résultats par page</label>
      <input class="input" type="number" name="rows" id="rows" value="1000">
   </div>
</fieldset>
<p><input type="submit" class="button is-primary" value="Executé la requête et afficher le résultat"></p>
</form>
<div id="search-dialog" class="content">
</div>
<div id="dialog" class="content">
</div>

<?php if ($parameter->queryBase!=''): ?>

<?php if(($_SESSION['nbResult']??1)==0):?>
<span style="color: #D8000C;">Attention, le résultat requête ne contient pas de résultat.</span>
<?php endif; ?>

<?php endif; ?>
</div>

<?php require 'footer.php'; ?>

</body>
<script>
   var engine = new liquidjs.Liquid({
      root: ['views/'],
      extname: '.liquid'
   });
   var queryDomain = "";
   var queryDomainLevel = -1;
   var currentQuery = null;
   $('#loadData').click(async ev => {
      ev.preventDefault();
      loadDataFromBrowser = await engine.renderFile("dialog/loadDataFromBrowser", {
         type: 'data',
         canSave: <?= (($parameter->queryBase!=''))?"true":"false" ?>
      });
      $("#dialog").html(loadDataFromBrowser);
      $("#dialog").dialog({width: "auto",'title': 'gestion des enregistrement'});
   });
   $("#advencedSearch").click(async ev => {
      ev.preventDefault();
      advencedSearch = await engine.renderFile("dialog/" + $('#queryType').val() + "AdvencedSearch", {});
      title = {
         collection: 'd\'une collection',
         structure: 'd\'une structure',
         idhal: 'l\'idHal d\'une personne',
      }
      await $("#search-dialog").dialog({
         width: "80%",
         title: 'Recherche ' + title[$('#queryType').val()],
         position: { my: "top", at: "center top", of: window }
      });
      $('#search-dialog').html(advencedSearch);
   })
   $('#queryForm').submit(async ev => {
      ev.preventDefault();
      waitingDialog = await engine.renderFile("dialog/waitRequest", {});
      $('#dialog').html(waitingDialog);
      $('#dialog').dialog({
         width: "auto",
         modal: true,
         title:'chargement',
      });
      data = {
         queryType: $('#queryType').val(),
         queryBase: ($('#queryType').val()=='halto')?$('#line').val():$('#queryBase').val(),
         producedDateStart: $('#producedDateStart').val(),
         producedDateEnd: $('#producedDateEnd').val(),
         complement: $('#complement').val(),
         domain: queryDomain,
         domainLevel: queryDomainLevel,
         includeCustomField: $('#includeCustomField').prop('checked'),
         rows: $('#rows').val(),
         maxResult: $('#maxResult').prop('checked'),
         saveData: $('#saveData').prop('checked'),
         instance: $('#instance').val(),
         customQuery: $('#customQuery').val(),
         france: $('#france').prop('checked'),
         producedDateInterval: $('#producedDateInterval').prop('checked'),
         noIncoming: $('#noIncoming').prop('checked'),
      }
      selectDoctypes = $('#docTypes').val();
      if(!selectDoctypes.includes('')) {
         data['docTypes'] = '(' + selectDoctypes.join(',') + ')';
      } else {
         data['docTypes'] = '';
      }
      currentQuery = $.ajax({
         type: "post",
         data: data,
         success: data => {
            if(!location.pathname.endsWith('index.php')) {
               location.pathname = location.pathname + 'result/map.php';
            } else {
               location.pathname = location.pathname.replace('index.php', 'result/map.php')
            }
         },
         error: data => {
            if (data.statusText!='abort') {
               alert('l\'erreur suivante est survenue :\n' + data.responseText);
            }
         }
      })
   })

   $('#advencedSearchParameterLink').click(ev => {
      ev.preventDefault();
      $('#advencedSearchParameter').toggle();
   })

   function addNexLevelEvent() {
      $('#nextLevel').change(ev => {
         queryDomain = $('#nextLevel').val();
         queryDomainLevel = 1;
      })
   }

   $('#domainL0').change(ev => {
      queryDomain = $('#domainL0').val();
      queryDomainLevel = 0;
      if(queryDomain === '') {
         $('#subDomainList').empty()
      } else {
         $.ajax({
            type: 'get',
            url: 'api/getDomain.php',
            data: {
               level: 1,
               parent: $('#domainL0').val()
            },
            success: data => {
               $('#subDomainList').html('<li><div class="select"><select name="nextLevel" id="nextLevel"></div></li>');
               data.forEach(element => {
                  $('#nextLevel').append('<option value="'+element.value+'">'+element.name+'</option>')
               })
               addNexLevelEvent();
            }
         })
      }
   })

   $.ajax({
      url: 'data/instance.json',
      success: async data => {
         var options = [];
         await data['response']['docs'].forEach(element => {
            options.push(new Option(element.name, element.code))
         });
         options.sort((a, b) => a.text.localeCompare(b.text));
         $('#instance').append(options);
         $('#instance').val("<?= $parameter->instance ?>");
      }
   })
   $.ajax({
      url: 'data/doctype.json',
      success: async data => {
         await data['response']['result']['doc'].forEach(element => {
            $('#docTypes').append(new Option(element.str[1], element.str[0]));
         });
         currentValue = '<?= $parameter->docTypes ?>';
         if(currentValue !== '') {
            var listValue = currentValue.substring(1, currentValue.length-1).split(',');
            $('#docTypes').val(listValue);
         }
      }
   })
   <?php if(isset($haltoDB)): ?>
   $('#haltoSelector').hide();
   $('#queryType').change(ev => {
      if($('#queryType').val() == 'halto') {
         $('#haltoSelector').show();
         $('#queryBaseInput').hide();
      } else {
         $('#haltoSelector').hide();
         $('#queryBaseInput').show();
      }
   })
   $('#queryType').change().trigger();
   <?php endif; ?>
</script>
</html>
