<?php
namespace uga\globhal\query;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use stdClass;

/**
 * 
 * Implémentation de DataResult pour les types de documents
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


class DocTypeDataResult extends DataResult {
    public const NEEDED_HAL_FIELD = ['docType_s'];
    public const NAME = 'doctype';
    public const MAPPING = ['docTypeList' => 'name'];
    protected static array $dataEntries = [];
    protected static array $onCreateCollable = [];

    protected string $name;

    public static function keyFromEntry(stdClass $entry, int $index=-1) {
        return $entry->docType_s;
    }

    public static function addEntryData(stdClass $entry, QueryParameter $parameter) {
        $newDocType = static::getOrCreate($entry);
        $newDocType->name = $entry->docType_s;
        $newDocType->callOnCreate($entry, $parameter);
    }
}