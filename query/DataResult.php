<?php
namespace uga\globhal\query;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\queryDefinition\DataContainer;
use uga\globhal\query\QueryResult;
use stdClass;

/**
 * 
 * Class abstraite pour la gestion d'une partie des résultats de la requête
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * Contient les champs de requête nécéssaire et un ensemble de donnée.
 */
abstract class DataResult extends DataContainer {
    public const NEEDED_HAL_FIELD = [];
    public const NAME = '';
    public const MAPPING = [];
    protected static array $dataEntries = [];
    protected static array $onCreateCollable = [];
    protected static QueryResult $queryResult;

    public static function setQueryResult(QueryResult $queryResult) {
        static::$queryResult = $queryResult;
    }

    /**
     * ajoute les donnée à partir d'une entré dans une requête
     *
     * @param stdClass $entry
     * @param QueryParameter $parameter
     * @return void
     */
    public static function addEntryData(stdClass $entry, QueryParameter $parameter) {}

    /**
     * permet d'extraire une clef d'une entry avec un index pour les champs a résultats multiple
     *
     * @param stdClass $entry
     * @param integer $index
     * @return void
     */
    public static function keyFromEntry(stdClass $entry, int $index=-1) {}

    public static function addOnCreateCollable(Callable $newOnCreate) {
        if(!in_array($newOnCreate, static::$onCreateCollable)) {
            array_push(static::$onCreateCollable, $newOnCreate);
        }
    }

    /**
     * permet de mapper ver un tableau key => value
     *
     * @param string $key
     * @return void
     */
    public function getKeyMapping(string $key) {
        return null;
    }

    public static function clean() {
        static::$dataEntries = [];
    }

    public function callOnCreate($entry, $parameter) {
        foreach(static::$onCreateCollable as $onCreate) {
            $onCreate($entry, $this, $parameter);
        }
    }

    public static function getOrCreate(stdClass $entry, int $index=-1) {
        $key = static::keyFromEntry($entry, $index);
        if($key!=null) {
            if(key_exists($key, static::$dataEntries)) {
                return static::$dataEntries[$key];
            } else {
                static::$dataEntries[$key] = new static();
                return static::$dataEntries[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * permet d'optenir un enssemble de tableau contenant toute les données
     *
     * @return array
     */
    public static function dataToArray(): array {
        $result = [];
        foreach(array_keys(static::MAPPING) as $target) {
            $result[$target] = [];
        }
        foreach(static::$dataEntries as $dataEntry) {
            foreach(static::MAPPING as $target => $key) {
                if($dataEntry->getKeyMapping($key)==null) {
                    array_push($result[$target], $dataEntry->$key);
                } else {
                    $result[$target][$dataEntry->getKeyMapping($key)] = $dataEntry->$key;
                }
            }
        }
        return $result;
    }
}
