<?php
namespace uga\globhal\query;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\halto\HaltoDB;
use uga\hallib\queryDefinition\DataContainer;
use uga\hallib\queryDefinition\IntervalElement;
use uga\hallib\queryDefinition\LiteralElement;
use uga\hallib\queryDefinition\QueryTreeElement;
use uga\hallib\search\SearchField;
use uga\hallib\search\SearchQuery;

/**
 * 
 * Gestion des paramétres et construction des requêtes.
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


class QueryParameter extends DataContainer {
    // paramétre de la requête

    /**
     * Type de requête parmis les requête par défaut
     *
     * @var string
     */
    protected string $queryType = 'collection';

    /**
     * Valeur de la requête
     *
     * @var string
     */
    protected string $queryBase = '';

    /**
     * complément optionnel de la requête
     *
     * @var string
     */
    protected string $complement = '';

    /**
     * restriction sur un domaine
     *
     * @var string
     */
    protected string $domain = '';

    /**
     * niveau du domaine de restriction
     *
     * @var integer
     */
    protected int $domainLevel = -1;

    /**
     * utilisé un intervale de date
     *
     * @var boolean
     */
    protected bool $producedDateInterval = false;

    /**
     * début de l'interval de date
     *
     * @var integer
     */
    protected int $producedDateStart = 2019;

    /**
     * fin de l'interval de date
     *
     * @var integer
     */
    protected int $producedDateEnd = 2022;

    /**
     * limité la requête à un nombre maximum de résultats
     *
     * @var boolean
     */
    protected bool $maxResult = false;

    /**
     * nombre de résultat par page ou maximum
     *
     * @var integer
     */
    protected int $rows = 1000;
    
    /**
     * portail ou effectuer la requête
     *
     * @var string
     */
    protected string $instance = '';

    /**
     * liste des types de documents
     *
     * @var string
     */
    protected string $docTypes = '';
    
    /**
     * requête personnalisée
     *
     * @var string
     */
    protected string $customQuery = '';


    // paramétre de traitement

    /**
     * Utilisé les pays personnalisé
     *
     * @var boolean
     */
    protected bool $includeCustomField = true;

    /**
     * évite de reconté plusieur foi une institution pour un même document
     *
     * @var boolean
     */
    protected bool $noRecount = true;

    /**
     * ignorer ou non les institution marqué comme fr
     *
     * @var boolean
     */
    protected bool $france = true;

    /**
     * sauvegarder les données
     *
     * @var boolean
     */
    protected bool $saveData = false;

    /**
     * Exclure les structure incomming
     *
     * @var boolean
     */
    protected bool $noIncoming = true;

    public function getLineAcronym(): string {
        $haltoDBFile = dirname(__FILE__, 2).'/data/halto_db/halto.db';
        if($this->queryType === 'halto' && file_exists($haltoDBFile)) {
            $haltoDB = new HaltoDB();
            $haltoDB->setDatabase($haltoDBFile);
            $line = $haltoDB->selectLine(intval($this->queryBase));
            if($line !== null) {
                return $line['acronym'];
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    /**
     * construit la requête correspondante aux paramétres
     *
     * @return SearchQuery
     */
    public function buildQuery(): SearchQuery {
        $query = new SearchQuery([
            'rows' => $this->rows,
            'instance' => $this->instance,
            'useCursor' => !$this->maxResult
        ]);
        $domain = $this->domain;
        $domainLevel = $this->domainLevel;
        $queryPart = '';
        $queryBase = urlencode($this->queryBase);
        if($this->customQuery == '') {
            switch ($this->queryType) {
                case "collection":
                    $query->collection = $queryBase;
                    $query->baseQuery = new LiteralElement([
                        'value' => '*'
                    ]);
                    break;
                case "structure":
                    $query->baseQuery = new LiteralElement([
                        'field' => SearchField::getVarient('structAcronym_s'),
                        'value' => $queryBase
                    ]);
                    break;
                case "idhal":
                    if(intval($this->queryBase) > 0) {
                        $fieldName = 'authIdHal_i';
                    } else {
                        $fieldName = 'authIdHal_s';
                    }
                    $query->baseQuery = new LiteralElement([
                        'field' => SearchField::getVarient($fieldName),
                        'value' => $queryBase
                    ]);
                    break;
                case 'halto':
                    $haltoDBFile = dirname(__FILE__, 2).'/data/halto_db/halto.db';
                    if(file_exists($haltoDBFile)) {
                        $haltoDB = new HaltoDB();
                        $haltoDB->setDatabase($haltoDBFile);
                        $query->baseQuery = $haltoDB->createQueryElement($this->queryBase);
                    }
                    break;
            }
        } else {
            $query->baseQuery = new LiteralElement([
                'value' => $this->customQuery,
                'escape' => false
            ]);
        }
        if($domain!=''&&$domainLevel!=-1) {
            $query->addFilter(new LiteralElement([
                'value' => 'level'.$domainLevel.'_domain_s:'.$domain,
                'escape' => false
            ]));
        }
        if($this->docTypes !== '') {
            $doctypes = explode(',', $this->docTypes);
            if(count($doctypes) <= 1) {
                $query->addFilter(new LiteralElement([
                    'value' => $this->docTypes,
                    'field' => SearchField::getVarient('docType_s')
                ]));
            } else {
                $docTypesElement = new QueryTreeElement([
                    'field' => SearchField::getVarient('docType_s'),
                ]);
                foreach($doctypes as $docType) {
                    $docTypesElement->addElement(
                        new LiteralElement([
                            'value' => $docType,
                        ])
                    );
                }
                $query->addFilter($docTypesElement);
            }
        }
        if($this->producedDateInterval) {
            $query->addFilter(new IntervalElement([
                'minValue' => $this->producedDateStart,
                'maxValue' => $this->producedDateEnd,
                'field' => SearchField::getVarient('producedDateY_i')
            ]));
        }
        return $query;
    }
}
