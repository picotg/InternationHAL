<?php
namespace uga\globhal\query;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use uga\hallib\ref\domain\DomainSelector;
use stdClass;

/**
 * 
 * Implémentation de DataResult pour les domaines et les pluridisciplinarité.
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class CrossDomain {
    private DomainSelector $domainSelector;
    private array $domainList;

    public function __construct($domainList, $domainSelector=null) {
        if($domainSelector==null) {
            $this->domainSelector = new DomainSelector();
            $this->domainSelector->extractData();
        } else {
            $this->domainSelector = $domainSelector;
        }
        $this->domainList = $domainList;
    }

    public function getDomainsCounts($level) {
        return count($this->getDomains());
    }

    public function getDomains() {
        $parents = [];
        $domains = [];
        foreach(array_reverse($this->domainList) as $levelDomains) {
            foreach($levelDomains as $domain) {
                $parents = array_merge($this->domainSelector->getParents($domain, true), $parents);
                if(!in_array($domain, $parents)&&!in_array($domain, $domains)) {
                    array_push($domains, $domain);
                }
            }
        }
        sort($domains);
        return $domains;
    }
}

class DomainList {
    private array $domainList = [[], [], [], []];
    private DomainSelector $domainSelector;
    private bool $count;

    public function __construct($count=false) {
        $this->domainSelector = new DomainSelector();
        $this->domainSelector->extractData();
        $this->count = $count;
    }
    
    public function addToDomainList($domain) {
        if(isset($this->domainSelector->code2doc($domain)->level_i)) {
            $level = $this->domainSelector->code2doc($domain)->level_i;
        } else {
            $level = 0;
        }
        if($domain != '') {
            if(!in_array($domain, $this->domainList[$level])) {
                if($this->count) {
                    $domain = ['name' => $domain, 'nb' => 1];
                }
                array_push($this->domainList[$level], $domain);
            } elseif ($this->count) {
                $this->domainList[$level][$domain]['nb'] += 1;
            }
        }
    }

    public function getCrossDomain() {
        $domainList = $this->domainList;
        if ($this->count) {
            $domainList = [[], [], [], []];
            foreach($this->domainList as $level => $domainLevelList) {
                foreach($domainLevelList as $domain) {
                    array_push($domainList[$level], $domain['name']);
                }
            }
        }
        return new CrossDomain($domainList, $this->domainSelector);
    }

    public function getDomainListData() {
        return $this->domainList;
    }
}

class DomainDataResult extends DataResult {
    public const NEEDED_HAL_FIELD = ['level0_domain_s', 'level1_domain_s', 'level2_domain_s', 'level3_domain_s', 'domainAllCode_s'];
    public const NAME = 'domain';
    public const MAPPING = [];
    protected static array $onCreateCollable = [];

    protected static DomainList $domainList;
    protected static DomainSelector $domainSelector;
    protected static array $domainsL0 = [];
    protected static array $domainCountryList = [];
    protected static array $domainCountryListL0 = [];
    protected static array $countryCrossDomains = [];
    protected static array $countryCrossDomainsL0 = [];
    protected static array $institutionCrossDomains = [];
    protected static array $institutionDomainsL0 = [];
    protected static array $crossDomainsList = [];

    public static function clean() {
        static::$domainSelector = new DomainSelector();
        static::$domainSelector->extractData();
        static::$domainList = new DomainList();
    }

    public static function dataToArray(): array {
        $result = [];
        $result['domainList'] = static::$domainList->getDomainListData();
        $result['domainCountryList'] = static::$domainCountryList;
        $result['domainCountryListL0'] = static::$domainCountryListL0;
        $result['countryCrossDomains'] = static::$countryCrossDomains;
        $result['countryCrossDomainsL0'] = static::$countryCrossDomainsL0;
        $result['institutionCrossDomains'] = static::$institutionCrossDomains;
        $result['institutionDomainsL0'] = static::$institutionDomainsL0;
        $result['crossDomainsList'] = static::$crossDomainsList;
        $result['domainsL0'] = static::$domainsL0;
        return $result;
    }

    public static function addEntryData(stdClass $entry, QueryParameter $parameter) {
        // construction list des domaine

        // liste des domaine pour le document courant
        $domainEntry = new DomainList(true);

        // construction des liste de domaine
        foreach([0, 1, 2, 3] as $level) {
            $levelFieldName = 'level'.$level.'_domain_s';
            if(isset($entry->$levelFieldName)&&is_array($entry->$levelFieldName)) {
                foreach($entry->$levelFieldName as $domain) {
                    if($level === 0 && !in_array($domain, static::$domainsL0)) {
                        array_push(static::$domainsL0, $domain);
                    }
                    static::$domainList->addToDomainList($domain);
                    $domainEntry->addToDomainList($domain);
                    foreach(static::$domainSelector->getParents($domain, true) as $parent) {
                        static::$domainList->addToDomainList($parent);
                        $domainEntry->addToDomainList($parent);
                    }
                }
                if($level === 0) {
                    if(count($entry->$levelFieldName) >= 2) {
                        $entry->crossDomainL0 = $entry->$levelFieldName;
                    }
                }
            }
        }

        $crossDomain = $domainEntry->getCrossDomain();
        $entry->crossDomain = $crossDomain->getDomains();
    }

    public static function connexionInstitution($entry, $institution, $parameter) {
        if(!array_key_exists($institution->id, static::$institutionCrossDomains)) static::$institutionCrossDomains[$institution->id] = [];
        if(count($entry->crossDomain) >= 2) {
            $crossDomains = implode(', ', $entry->crossDomain);
            if(!array_key_exists($crossDomains, static::$crossDomainsList)) array_push(static::$crossDomainsList, $crossDomains);
            if(!array_key_exists($institution->id, static::$institutionCrossDomains[$institution->id])) {
                static::$institutionCrossDomains[$institution->id][$crossDomains] = 1;
            } else {
                static::$institutionCrossDomains[$institution->id][$crossDomains] += 1;
            }
        }
        if(!key_exists($institution->id, static::$institutionDomainsL0)) {
            static::$institutionDomainsL0[$institution->id] = [];
        }
        if (is_array($entry->level0_domain_s)) {
            foreach($entry->level0_domain_s as $L0Domain) {
                if(!in_array($L0Domain, static::$institutionDomainsL0[$institution->id])) {
                    array_push(static::$institutionDomainsL0[$institution->id], $L0Domain);
                }
            }
        }
    }

    protected static function addToCrossDomain($country, $crossDomainEntry, &$crossDomainArray) {
        if(is_array($crossDomainEntry)) {
            $crossDomainEntry = array_unique($crossDomainEntry);
            sort($crossDomainEntry);
            if(!array_key_exists($country->code, $crossDomainArray)) $crossDomainArray[$country->code] = [];
            if(count($crossDomainEntry) >= 2) {
                $crossDomains = implode(', ', $crossDomainEntry);
                $crossDomainArray[$country->code][$crossDomains]['names'] = [];
                foreach($crossDomainEntry as $domain) {
                    if(isset(static::$domainSelector->code2doc($domain)->fr_domain_s)) {
                        array_push($crossDomainArray[$country->code][$crossDomains]['names'], DomainSelector::cleanDomainName(static::$domainSelector->code2doc($domain)->fr_domain_s));
                    }
                }
                if(!array_key_exists($crossDomains, $crossDomainArray[$country->code])) {
                    $crossDomainArray[$country->code][$crossDomains]['nb'] = 1;
                } else {
                    if(!array_key_exists('nb', $crossDomainArray[$country->code][$crossDomains])) {
                        $crossDomainArray[$country->code][$crossDomains]['nb'] = 1;
                    }
                    $crossDomainArray[$country->code][$crossDomains]['nb'] += 1;
                }
            }
        }
    }

    public static function connexionCountry($entry, $country, $parameter) {
        if(!isset(static::$domainCountryList[$country->code])||!is_array(static::$domainCountryList[$country->code])) {
            static::$domainCountryList[$country->code] = [];
        }
        foreach($entry->crossDomain as $domain) {
            if(!array_key_exists($domain, static::$domainCountryList[$country->code])) {
                static::$domainCountryList[$country->code][$domain] = 1;
            } else {
                static::$domainCountryList[$country->code][$domain] += 1;
            }
        }
        if(!key_exists($country->code, static::$domainCountryListL0)) {
            static::$domainCountryListL0[$country->code] = [];
        }
        if (is_array($entry->level0_domain_s)) {
            foreach($entry->level0_domain_s as $L0Domain) {
                if(!in_array($L0Domain, static::$domainCountryListL0[$country->code])) {
                    array_push(static::$domainCountryListL0[$country->code], $L0Domain);
                }
            }
        }
        static::addToCrossDomain($country, $entry->crossDomain, static::$countryCrossDomains);
        static::addToCrossDomain($country, $entry->level0_domain_s, static::$countryCrossDomainsL0);
    }
}
