<?php
namespace uga\globhal\query;

use uga\globhal\data\Countries;

chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * Implémentation de DataResult pour les pays
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * DataResult pour les pays.
 */
class CountryDataResult extends DataResult {
    protected static array $dataEntries = [];
    protected static array $onCreateCollable = [];
    public const NEEDED_HAL_FIELD = ['instStructId_i'];
    public const MAPPING = ['countryList' => 'code', 'countryInstitutions' => 'Institutions'];
    public const NAME = 'country';
    
    protected string $code;
    protected array $Institutions = [];

    public function getKeyMapping(string $key) {
        if($key=='Institutions'){
            return $this->code;
        }
        return null;
    }

    public static function createFromInstitution($entry, $institution, QueryParameter $parameter) {
        $code = Countries::getKnownCode($institution->country);
        if($code!="fr"||$parameter->france) {
            if(key_exists($code, static::$dataEntries)) {
                $newEntry = static::$dataEntries[$code];
            }
            else {
                $newEntry = static::$dataEntries[$code] = new static();
                $newEntry->code = $code;
            }
            if(!in_array($institution->id, $newEntry->Institutions)) {
               $newEntry->Institutions[] = $institution->id;
            }
            $newEntry->callOnCreate($entry, $parameter);
        }
    }
}
