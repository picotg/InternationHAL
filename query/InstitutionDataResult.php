<?php
namespace uga\globhal\query;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use stdClass;
use uga\globhal\data\DBStruct;

/**
 * 
 * Implémentation de DataResult pour les institutions
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class InstitutionDataResult extends DataResult {
    public const NEEDED_HAL_FIELD = ['instStructId_i'];
    public const NAME = 'institution';
    public const MAPPING = [
        'entryListByInstitution' => 'entries',
        'institutionDocTypeList' => 'docTypes',
        'institutionList' => 'nbEntry',

    ];
    protected static array $dataEntries = [];
    protected static array $onCreateCollable = [];
    protected static int $maxNbEntry = 0;
    protected static DBStruct $dbStruct;

    protected string $country;
    protected array $counted = [];
    protected array $docTypes = [];
    protected array $entries = [];
    protected int $id;
    protected int $nbEntry = 0;

    public static function initDB() {
        static::$dbStruct = new DBStruct();
    }

    public static function getMaxNbEntry() {
        return static::$maxNbEntry;
    }

    public function getKeyMapping(string $key) {
        return $this->id;
    }

    public static function keyFromEntry(stdClass $entry, int $index=-1) {
        return $index;
    }

    public static function addEntryData(stdClass $entry, QueryParameter $parameter) {
        foreach($entry->instStructId_i as $institutionId) {
            $data = static::$dbStruct->getStruct($institutionId);
            if($data == null) continue;
            if(strtolower($data['country'])!="fr" or $parameter->france) {
                if($data['valid'] == 'INCOMING' and $parameter->noIncoming) {
                    continue;
                }
                // déjà conté pour cette publication (évite de conté plusieurs foi une institution lié plusieurs foi à une même pulbication)
                $newInstitution = static::getOrCreate($entry, $institutionId);
                $newInstitution->id = $institutionId;
                $newInstitution->country = strtolower($data['country']);
                if(!key_exists($entry->docid, $newInstitution->counted)) {
                    $newInstitution->counted[$entry->docid] = false;
                    $newEntrie = new stdClass();
                    $newEntrie->docid = $entry->docid;
                    $newEntrie->docType_s = $entry->docType_s;
                    $newEntrie->domainAllCode_s = $entry->domainAllCode_s;
                    $newEntrie->level0_domain_s = $entry->level0_domain_s;
                    $newEntrie->level1_domain_s = $entry->level1_domain_s;
                    $newEntrie->level2_domain_s = $entry->level2_domain_s;
                    $newEntrie->level3_domain_s = $entry->level3_domain_s;
                    $newEntrie->crossDomain = $entry->level0_domain_s;
                    $newInstitution->entries[] = $newEntrie;
                }
                if(!$newInstitution->counted[$entry->docid]) {
                    // construit la liste des nombre de type de document par institutions
                    if (array_key_exists($entry->docType_s, $newInstitution->docTypes)) {
                        $newInstitution->docTypes[$entry->docType_s] += 1;
                    } else {
                        $newInstitution->docTypes[$entry->docType_s] = 1;
                    }
                    $newInstitution->nbEntry += 1;
                    if($newInstitution->nbEntry > static::$maxNbEntry) {
                        static::$maxNbEntry = $newInstitution->nbEntry;
                    }
                }
                $newInstitution->callOnCreate($entry, $parameter);
                $newInstitution->counted[$entry->docid] = $parameter->noRecount;
            }
        }
    }
}