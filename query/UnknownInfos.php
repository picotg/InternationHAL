<?php
namespace uga\globhal\query;

use uga\globhal\data\Countries;
use uga\globhal\data\DBStruct;
use uga\hallib\queryDefinition\DataContainer;

/**
 * 
 * Gestion des informations inconnu
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Modélisation des information inconnues sur une institution
 */
class UnknownInfoInstitution extends DataContainer {
    protected string $name = 'inc';
    protected string $country = 'inc';
    protected ?string $customCountry = null;
    protected ?string $customName = null;
    protected string $institutionId = '';
    protected static array $excludeList = ['customCountry', 'customName', 'institutionId', 'docId'];

    public function __get(string $name) {
        if(substr($name, 0, strlen('custom')) === 'custom') {
            if($this->$name===null&&isset($_SESSION['custom_data'][$this->institutionId][$name])) {
                $this->$name = $_SESSION['custom_data'][$this->institutionId][$name];
            }
            return $this->$name;
        }
        return parent::__get($name);
    }

    public function __set(string $name, $value) {
        if(substr($name, 0, strlen('custom')) === 'custom') {
            $_SESSION['custom_data'][$this->institutionId][$name] = $value;
        }
        parent::__set($name, $value);
    }

    public function __isset($name) {
        if(substr($name, 0, strlen('custom')) === 'custom') {
            return $this->$name !== null||isset($_SESSION['custom_data'][$this->institutionId][$name]);
        } else {
            if(isset($this->$name)) {
                if($this->$name == 'inc') {
                    return false;
                }
            }
        }
        return parent::__isset($name);
    }

    public static function createFromeDB($institutionId, DBStruct $dbStruct) {
        $structInfos = $dbStruct->getStruct(intval($institutionId));
        $newInfos = new UnknownInfoInstitution($structInfos);
        $newInfos->institutionId = $institutionId;
        return $newInfos;
    }

    public function __unset($name) {
        if(substr($name, 0, strlen('custom')) === 'custom') {
            unset($_SESSION['custom_data'][$this->institutionId][$name]);
        }
    }
}

class UnknownInfos {
    private array $infos = [];
    private DBStruct $dbStruct;

    public function __construct() {
        $this->dbStruct = new DBStruct();
    }

    public function getUnknownList(array $institutionList): array {
        $findUnknows = [];
        foreach($institutionList as $institutionId => $nb) {
            $data = $this->dbStruct->getStruct($institutionId);
            if($data['name'] == null || $data['country'] == null || Countries::getKnownCode($data['country']) == 'inc') {
                $findUnknows[] = $institutionId;
            }
        }
        return $findUnknows;
    }

    public function __get($institutionId) {
        if(!array_key_exists($institutionId, $this->infos)) {
            $this->infos[$institutionId] = UnknownInfoInstitution::createFromeDB($institutionId, $this->dbStruct);
        }
        return $this->infos[$institutionId];
    }

    public function __set($institutionId, $value) {
        $this->infos[$institutionId] = new UnknownInfoInstitution($value);
        $this->infos[$institutionId]->institutionId = $institutionId;
        $this->infos[$institutionId]->saveToFile();
    }

    public function __isset($institutionId) {
        return $this->__get($institutionId) !== null;
    }

    public function __unset($institutionId) {
        if(array_key_exists($institutionId, $this->infos)) {
            $this->infos[$institutionId]->delete();
            unset($this->infos[$institutionId]);
        }
    }
}
