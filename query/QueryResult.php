<?php
namespace uga\globhal\query;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

use stdClass;
use uga\hallib\queryDefinition\DataContainer;
use uga\hallib\QueryIterator;
use uga\hallib\search\SearchQuery;
use uga\globhal\query\QueryParameter;
use uga\globhal\query\DataResult;
use uga\globhal\query\CountryDataResult;
use uga\globhal\query\InstitutionDataResult;
use uga\globhal\query\EntryDataResult;
use uga\globhal\query\DomainDataResult;
use uga\globhal\query\DocTypeDataResult;

/**
 * 
 * résultat de la requête.
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

class QueryResult extends DataContainer {
    /**
     * set de données
     *
     * @var array
     */
    private array $dataSets=[];

    /**
     * paramètre de la requête
     *
     * @var QueryParameter
     */
    protected QueryParameter $parameter;
    
    /**
     * Données brute
     *
     * @var array
     */
    protected array $rowData = [];

    /**
     * requête executé
     *
     * @var SearchQuery
     */
    protected SearchQuery $query;

    /**
     * nombre maximum de documents par institution
     *
     * @var integer
     */
    protected int $institutionMax = 0;

    /**
     * nombre de résultat trouver
     *
     * @var integer
     */
    protected int $nbResult = 0;

    /**
     * Dernière execution de la requête
     *
     * @var integer
     */
    protected int $lastExectedTimestemp = 0;

    public function clean() {
        foreach($this->dataSets as $dataSet) {
            $dataSet::clean();
        }
    }

    public function getSet($name) {
        return $this->dataSets[$name];
    }

    public function addSet($set) {
        $set::clean();
        foreach($this->dataSets as $otherDataResult) {
            $name = 'createFrom'.ucfirst($set::NAME);
            if(method_exists($otherDataResult, $name)) {
                $set::addOnCreateCollable([$otherDataResult, $name]);
            }
            $name = 'createFrom'.ucfirst($otherDataResult::NAME);
            if(method_exists($set, $name)) {
                $otherDataResult::addOnCreateCollable([$set, $name]);
            }
            $name = 'connexion'.ucfirst($set::NAME);
            if(method_exists($otherDataResult, $name)) {
                $set::addOnCreateCollable([$otherDataResult, $name]);
            }
            $name = 'connexion'.ucfirst($otherDataResult::NAME);
            if(method_exists($set, $name)) {
                $otherDataResult::addOnCreateCollable([$set, $name]);
            }
        }
        $this->dataSets[$set::NAME] = $set;
    }

    public function getNeededHalField(): array {
        $result = [];
        foreach($this->dataSets as $dataSet) {
            $result = array_merge($result, $dataSet::NEEDED_HAL_FIELD);
        }
        return $result;
    }

    public function __construct(QueryParameter $parameter, ?SearchQuery $query=null) {
        DataResult::setQueryResult($this);
        $this->addSet(CountryDataResult::class);
        $this->addSet(DocTypeDataResult::class);
        $this->addSet(DomainDataResult::class);
        InstitutionDataResult::initDB();
        $this->addSet(InstitutionDataResult::class);
        $this->addSet(EntryDataResult::class);
        $this->excludeList = ['parameter'];
        $this->query = ($query!==null)?$query:$parameter->buildQuery();
        $this->parameter = $parameter;
    }

    public function getDataArray(): array {
        // enregistrer les résultats de la requête dans la session.
        $thisArray = $this->toArray();
        foreach($this->dataSets as $dataSet) {
            $thisArray = array_merge($thisArray, $dataSet::dataToArray());
        }
        return $thisArray;
    }

    public function extractAllData($executequery=true) {
        if($executequery) {
            $this->clean();
            $this->query->addReturnedFields($this->getNeededHalField());
            $dataIterator = new QueryIterator($this->query);
            $this->lastExectedTimestemp = time();
        } else {
            $dataIterator = $this->rowData;
        }
        foreach($dataIterator as $entry) {
            if($executequery) $newEntry = json_decode($entry->json);
            else $newEntry = $entry;
            if($this->parameter->saveData) array_push($this->rowData, $newEntry);
            if(!isset($newEntry->instStructId_i)||!is_array($newEntry->instStructId_i)) continue;
            foreach($this->dataSets as $dataSet) {
                $dataSet::addEntryData($newEntry, $this->parameter);
            }
        }
        if($executequery) {
            $this->nbResult = $dataIterator->getNbResult();
        } else {
            $this->nbResult = count($this->rowData);
        }
        $this->institutionMax = InstitutionDataResult::getMaxNbEntry();
    }

    public function getNbResult() {
        return $this->nbResult;
    }
}