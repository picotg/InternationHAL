<?php
namespace uga\globhal\api;

use Exception;
use stdClass;
use uga\globhal\data\Countries;
use uga\globhal\data\DBStruct;
use uga\globhal\query\UnknownInfos;
use uga\hallib\ref\domain\DomainSelector;

/**
 * 
 * Génére les donnée de la carte pour la session courante.
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

$modeList = ['domain', 'crossDomain', 'institution'];

$unselected = [];
$ignorList = [];
foreach($modeList as $mode) {
   if(isset($_GET['selected'.ucfirst($mode)])) {
      $unselected[$mode] = !$_GET['selected'.ucfirst($mode)];
   } else {
      $unselected[$mode] = true;
   }
   if(isset($_SESSION[$mode.'IgnorList'])) {
      $ignorList[$mode] = $_SESSION[$mode.'IgnorList'];
   } else {
      $ignorList[$mode] = [];
   }
}

// verification de la présence du resultat de la requête en session.
if(!isset($_SESSION['resultRequest'])) {
   throw new Exception('Vous devez lancer une recherche avant d\'ouvrir ce fichier.');
}

header('Content-Type: application/json ');
header('Content-Encoding: UTF-8');
header('Content-type: application/json; charset=UTF-8');
header('Content-disposition: attachment;filename=map.json');
echo "\xEF\xBB\xBF"; // UTF-8 BOM

$domainSelector = new DomainSelector();
$domainSelector->extractData();

$resultRequest = $_SESSION['resultRequest'];

$countryList = $resultRequest["countryList"];
$institutionList = $resultRequest["institutionList"];
$countryInstitutions = $resultRequest["countryInstitutions"];
$institutionMax = $resultRequest["institutionMax"];
$domainCountryList = $resultRequest["domainCountryList"];
$countryCrossDomains = $resultRequest["countryCrossDomains"];
$countryCrossDomainsL0 = $resultRequest["countryCrossDomainsL0"];


$parameter = $_SESSION['parameter'];
if($parameter['includeCustomField']) {
   function addToCrossDomain($country, $crossDomainEntry, &$crossDomainArray) {
      $domainSelector = new DomainSelector();
      if(is_array($crossDomainEntry)) {
          $crossDomainEntry = array_unique($crossDomainEntry);
          sort($crossDomainEntry);
          if(!array_key_exists($country, $crossDomainArray)) $crossDomainArray[$country] = [];
          if(count($crossDomainEntry) >= 2) {
              $crossDomains = implode(', ', $crossDomainEntry);
              $crossDomainArray[$country][$crossDomains]['names'] = [];
              foreach($crossDomainEntry as $domain) {
                  if(isset($domainSelector->code2doc($domain)->fr_domain_s)) {
                      array_push($crossDomainArray[$country][$crossDomains]['names'], DomainSelector::cleanDomainName($domainSelector->code2doc($domain)->fr_domain_s));
                  }
              }
              if(!array_key_exists($crossDomains, $crossDomainArray[$country])) {
                  $crossDomainArray[$country][$crossDomains]['nb'] = 1;
              } else {
                  if(!array_key_exists('nb', $crossDomainArray[$country][$crossDomains])) {
                      $crossDomainArray[$country][$crossDomains]['nb'] = 1;
                  }
                  $crossDomainArray[$country][$crossDomains]['nb'] += 1;
              }
          }
      }
   }
   $entryListByInstitution = $resultRequest["entryListByInstitution"];
   $unknownInfos = new UnknownInfos();
   $findUnknow = $unknownInfos->getUnknownList($institutionList);
   foreach($findUnknow as $institutionId) {
      $data = $unknownInfos->$institutionId;
      if (isset($data->customCountry)) {
         if(!isset($countryInstitutions[$data->customCountry])) {
            $countryInstitutions[$data->customCountry] = [];
         }
         if(!in_array($data->customCountry, $countryList)) {
            $countryList[] = $data->customCountry;
            $domainCountryList[$data->customCountry] = [];
            $countryCrossDomains[$data->customCountry] = [];
            $countryCrossDomainsL0[$data->customCountry] = [];
         }
         $countryInstitutions[$data->customCountry][] = $institutionId;
         foreach($entryListByInstitution[$institutionId] as $entry) {
            foreach($entry->crossDomain as $domain) {
               if(!array_key_exists($domain, $domainCountryList[$data->customCountry])) {
                  $domainCountryList[$data->customCountry][$domain] = 1;
               } else {
                  $domainCountryList[$data->customCountry][$domain] += 1;
               }
            }
            addToCrossDomain($data->customCountry, $entry->crossDomain, $countryCrossDomains);
            addToCrossDomain($data->customCountry, $entry->level0_domain_s, $countryCrossDomainsL0);
         }
      }
   }
}

$dbStruct = new DBStruct();

// source du fichier : https://www.datavis.fr/index.php?page=map-improve
$countriesGeoJson = json_decode(file_get_contents("data/world-countries-no-antartica.json"));

$dataGeoJson = new stdClass();
$dataGeoJson->features = [];
$dataGeoJson->type = "FeatureCollection";
$institutionNumberList = [];
$domainNumberList = [];
$crossDomainNumberList = [];
$crossDomainNumberL0List = [];

foreach($countriesGeoJson->features as $feature) {
   $country = strtolower($feature->id);
   if(in_array($country, $countryList)) {
      $countryName = Countries::getCountry($country);
      if($countryName == 'inconnu') continue;
      $newFeature = $feature;
      $newFeature->properties->name = $countryName;
      if(!$unselected['institution']) {
         $institutionNumber = 0;
         $newFeature->properties->institutions = [];   
         $thisCountryInstitutions = [];
         foreach($countryInstitutions[$country] as $institutionId) {
            if(!in_array($institutionId, $ignorList['institution'])) {
               $thisCountryInstitutions[$institutionId]=intval($institutionList[$institutionId]);
               $institutionNumber += 1;
            }
         }
         arsort($thisCountryInstitutions);
         foreach($thisCountryInstitutions as $institutionId => $numberDoc) {
            $newInstitution = new stdClass();
            $newInstitution->name = $dbStruct->getStruct(intval($institutionId))['name'];
            $newInstitution->id = $institutionId;
            $newInstitution->nb = $numberDoc;
            $newInstitution->pluralDoc = ($numberDoc>1)?"s":"";
            array_push($newFeature->properties->institutions, $newInstitution);
         }
         array_push($institutionNumberList, $institutionNumber);
         $newFeature->properties->institutionNumber = $institutionNumber;
         $newFeature->properties->pluralInstitutions = ($institutionNumber>1)?"s":"";
      }
      if (!$unselected['domain']) {
         $newFeature->properties->domains = [];
         foreach($domainCountryList[$country] as $domain => $nbDomain) {
            $domainDoc = $domainSelector->code2doc($domain);
            $domainName = isset($domainDoc->fr_domain_s)?DomainSelector::cleanDomainName($domainDoc->fr_domain_s):$domain;
            if(!in_array($domainName, $ignorList['domain']))
               array_push($newFeature->properties->domains, [
                  'name' => $domainName,
                  'nb' => $nbDomain,
                  'code' => $domain
               ]);
         }
         $newFeature->properties->domainNumber = count($newFeature->properties->domains);
         array_push($domainNumberList, $newFeature->properties->domainNumber);
      }
      if(!$unselected['crossDomain']) {
         $newFeature->properties->crossDomains = [];
         $newFeature->properties->crossDomainNumber = 0;
         foreach($countryCrossDomains[$country] as $crossDomain => $crossDomainDetails) {
            if(count($crossDomainDetails['names'])>=2&&!in_array($crossDomain, $ignorList['crossDomain'])) {
               $newFeature->properties->crossDomains += [$crossDomain => $crossDomainDetails];
               $newFeature->properties->crossDomainNumber += 1;
            }
         }
         foreach($countryCrossDomainsL0[$country] as $crossDomain => $crossDomainDetails) {
            if(count($crossDomainDetails['names'])>=2&&!in_array($crossDomain, $ignorList['crossDomain'])) {
               if(!isset($newFeature->properties->crossDomainsL0)) {
                  $newFeature->properties->crossDomainsL0 = [];
                  $newFeature->properties->crossDomainL0Number = 0;
               }
               $newFeature->properties->crossDomainsL0 += [$crossDomain => $crossDomainDetails];
               $newFeature->properties->crossDomainL0Number += 1;
            }
         }
         array_push($crossDomainNumberList, count($newFeature->properties->crossDomains));
         array_push($crossDomainNumberL0List, isset($newFeature->properties->crossDomainsL0)?count($newFeature->properties->crossDomainsL0):0);
      }
      array_push($dataGeoJson->features, $newFeature);
   }
}

$result = [
   'map' => $dataGeoJson,
   'values' => [],
   'parameter' => $parameter,
];
if(!$unselected['institution']) {
   sort($institutionNumberList);
   $result['values'] += ['institution' => $institutionNumberList];
}
if(!$unselected['domain']) {
   sort($domainNumberList);
   $result['values'] += ['domain' => $domainNumberList];
}
if(!$unselected['crossDomain']) {
   sort($crossDomainNumberList);
   $result['values'] += ['crossDomain' => $crossDomainNumberList];
   sort($crossDomainNumberL0List);
   $result['values'] += ['crossDomainL0' => $crossDomainNumberL0List];
}
echo json_encode($result);
