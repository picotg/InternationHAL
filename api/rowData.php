<?php
namespace uga\globhal\api;

use uga\globhal\query\QueryParameter;
use uga\globhal\query\QueryResult;

/**
 * 
 * load and download rowData
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

header('Content-Type: application/json ');
header('Content-Encoding: UTF-8');
header('Content-type: application/json; charset=UTF-8');
header('Content-disposition: attachment;filename=data.json');
echo "\xEF\xBB\xBF"; // UTF-8 BOM

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    session_reset();
    if(!isset($_POST['data'])) {
        echo '{"error": "data not upload"}';
        exit(0);
    }
    $data = json_decode($_POST['data']);
    if(!isset($data->parameter)) {
        echo '{"error": "parameter not in data"}';
        exit(0);
    }
    if(!isset($data->query)) {
        echo '{"error": "query not in data"}';
        exit(0);
    }
    $query = unserialize($data->query);
    $parameter = new QueryParameter($data->parameter);
    $qr = new QueryResult($parameter, $query);
    if(isset($data->rowData)) {
        $qr->rowData = $data->rowData;
        $qr->extractAllData(false);
    } else {
        $qr->extractAllData(true);
    }
    $_SESSION['resultRequest'] = $qr->getDataArray();
    $_SESSION['parameter'] = $parameter->toArray();
    echo '{}';
    exit(0);
} else if(isset($_SESSION['parameter'])) {
    $data = [];
    $parameter = new QueryParameter($_SESSION['parameter']);
    $resultRequest = $_SESSION['resultRequest'];
    $data['parameter'] = $parameter->toArray();
    $data['query'] = serialize($_SESSION['query']);
    if(isset($_GET['data'])&&$_GET['data']) {
        $data['rowData'] = $resultRequest['rowData'];
        if(isset($_GET['pretty'])) {
            echo json_encode($data, JSON_PRETTY_PRINT);
        } else {
            echo json_encode($data);
        }
    } else {
        if(isset($_GET['pretty'])) {
            echo json_encode($data, JSON_PRETTY_PRINT);
        } else {
            echo json_encode($data);
        }
    }
    exit(0);
}
echo '{"error": "noOp"}';
