<?php
namespace uga\globhal\api;

use uga\globhal\data\Countries;
use uga\globhal\data\DBStruct;

chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

/**
 * 
 * API pour accédé aux données des institutions.
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

header('Content-Type: application/json');
header('Content-Encoding: UTF-8');
header('Content-type: application/json; charset=UTF-8');
echo "\xEF\xBB\xBF"; // UTF-8 BOM

$dbStruct = new DBStruct();

if(isset($_GET['institutionId'])&&isset($_GET['country'])) {
    $institutionId = $_GET['institutionId'];
    $country = $_GET['country'];

    if((isset($_SESSION['resultRequest']))) {
        $institutionData = ['institution' => [
            'id' => $institutionId,
            'country' => Countries::getCountry($country),
            'valid' => $dbStruct->getStruct($institutionId)['valid'],
        ]];
        echo json_encode($institutionData);
        exit(0);
    } else {
        echo '{"error": "$_SESSION["resultRequest"] not set"}';
        exit(0);
    }
}


