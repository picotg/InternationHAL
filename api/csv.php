<?php
namespace uga\globhal\api;

use uga\globhal\data\Countries;
use uga\globhal\data\DBStruct;
use uga\globhal\query\UnknownInfos;
use uga\hallib\QueryIterator;
use uga\hallib\ref\domain\DomainSelector;

/**
 * 
 * Rendus sous forme de fichier CSV
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

// verification de la présence du resultat de la requête en session.
if(!isset($_SESSION['resultRequest'])) {
   echo('Vous devez lancer une recherche <a href="index.php">ici</a> avant d\'ouvrir ce fichier.');
   exit(0);
}

$filename = (isset($_GET['full'])&&$_GET['full'])?'full.csv':'summary.csv';
$header = (isset($_GET['header']))?$_GET['header']:true;
$countryCode = (isset($_GET['countryCode']))?$_GET['countryCode']:false;
$countryName = (isset($_GET['countryName']))?$_GET['countryName']:false;
$selectInstitutionId = (isset($_GET['institutionId']))?$_GET['institutionId']:false;
$institutionName = (isset($_GET['institutionName']))?$_GET['institutionName']:false;
$docid = (isset($_GET['docid']))?$_GET['docid']:false;
$nbPub = (isset($_GET['nbPub']))?$_GET['nbPub']:false;
$docType = (isset($_GET['docType']))?$_GET['docType']:false;
$domain = (isset($_GET['domainCSV']))?$_GET['domainCSV']:false;
$crossDomain = (isset($_GET['crossDomainCSV']))?$_GET['crossDomainCSV']:false;
$valid = (isset($_GET['valid']))?$_GET['valid']:false;
$docTitle = (isset($_GET['docTitle']))?$_GET['docTitle']:false;
$docAuth = (isset($_GET['docAuth']))?$_GET['docAuth']:false;
$domainList = (isset($_GET['domainList']))?$_GET['domainList']:false;


$domainSelector = new DomainSelector();
$domainSelector->extractData();

header('Content-Type: text/csv');
header('Content-Encoding: UTF-8');
header('Content-type: text/csv; charset=UTF-8');
header('Content-disposition: attachment;filename=' . $filename);
echo "\xEF\xBB\xBF"; // UTF-8 BOM

$resultRequest = $_SESSION['resultRequest'];

$newCountryList = $resultRequest["countryList"];
$institutionList = $resultRequest["institutionList"];
$countryInstitutions = $resultRequest["countryInstitutions"];
$institutionMax = $resultRequest["institutionMax"];
$entryListByInstitution = $resultRequest["entryListByInstitution"];
$institutionDocTypeList = $resultRequest["institutionDocTypeList"];
$docTypeList = $resultRequest["docTypeList"];
$institutionCrossDomains = $resultRequest["institutionCrossDomains"];
$crossDomainsList = $resultRequest["crossDomainsList"];
$domainsL0 = $resultRequest["domainsL0"];
$institutionDomainsL0 = $resultRequest["institutionDomainsL0"];

$dbStruct = new DBStruct();

$countryList = [];
foreach($newCountryList as $country) {
   $countryList[$country] = 0;
   foreach($countryInstitutions[$country] as $institutionId) {
      $countryList[$country] += 1;
   }
}

$infoAuthTitle = [];

if($docAuth || $docTitle) {
   $query = $_SESSION['resultRequest']['query'];
   $query->emptyReturnedFields();
   $query->addReturnedFields(['authFullName_s', 'title_s', 'docid']);
   $requestIterator = new QueryIterator($query);
   foreach($requestIterator as $doculrResult) {
      $infoAuthTitle[$doculrResult->docid] = [
         'titles' => join(', ', $doculrResult->title_s),
         'authors' => join(', ', $doculrResult->authFullName_s),
      ];
   }
}

$parameter = $_SESSION['parameter'];
if($parameter['includeCustomField']) {
   $entryListByInstitution = $resultRequest["entryListByInstitution"];
   $unknownInfos = new UnknownInfos();
   $findUnknow = $unknownInfos->getUnknownList($institutionList);
   foreach($findUnknow as $institutionId) {
      $data = $unknownInfos->$institutionId;
      if (isset($data->customCountry)) {
         if(($key = array_search($institutionId, $countryInstitutions['inc'])) !== false) {
            unset($countryInstitutions['inc'][$key]);
            $countryList['inc']--;
         }
         $countryInstitutions[$data->customCountry] ??= [];
         if(!in_array($data->customCountry, $countryList)) {
            $countryList[$data->customCountry] = 0;
         } else {
            $countryList[$data->customCountry]++;
         }
         $countryInstitutions[$data->customCountry][] = $institutionId;
      }
   }
}

$output = fopen('php://output', 'w');

if(isset($_GET['full'])&&$_GET['full']) {
   if($header) {
      $line = [];
      if ($countryCode) array_push($line, 'code pays');
      if ($countryName) array_push($line, 'nom pays');
      if ($selectInstitutionId) array_push($line, 'id institution');
      if ($institutionName) array_push($line, 'nom institution');
      if ($docid) array_push($line, 'docid');
      if ($docType) array_push($line, 'docType');
      if ($domain) array_push($line, 'domain');
      if ($crossDomain) array_push($line, 'pluridisciplinarité');
      if ($valid) array_push($line, 'validité institution');
      if ($docTitle) array_push($line, 'titre publication');
      if ($docAuth) array_push($line, 'auteurs publication');
      if ($domainList) {
         foreach($domainsL0 as $domainL0) {
            $domainL0Doc = $domainSelector->code2doc($domainL0);
            $domainNameL0 = isset($domainL0Doc->fr_domain_s)?DomainSelector::cleanDomainName($domainL0Doc->fr_domain_s):$domainL0;
            array_push($line, $domainNameL0);
         }
      }
      fputcsv($output, $line, $separator = ";");
   }
   foreach($countryList as $country => $number){
      foreach($countryInstitutions[$country] as $institutionId){
         $institutionData = $dbStruct->getStruct(intval($institutionId));
         $currentIntitution = $institutionData['name'];
         $currentValid = $institutionData['valid'];
         foreach($entryListByInstitution[$institutionId] as $entry) {
            $countryNameValue = Countries::getCountry($country);
            $line = [];
            if ($countryCode) array_push($line, $country);
            if ($countryName) array_push($line, $countryNameValue);
            if ($selectInstitutionId) array_push($line, $institutionId);
            if ($institutionName) array_push($line, $currentIntitution);
            if ($docid) array_push($line, $entry->docid);
            if ($docType) array_push($line, $entry->docType_s);
            if ($domain) {
               $allCode_s = '';
               if(isset($entry->domainAllCode_s) && is_array($entry->domainAllCode_s))
               {
                  foreach($entry->domainAllCode_s as $i => $code_s) {
                     if($i!=0) $allCode_s .= ',';
                     $allCode_s .= $code_s;
                  }
               }
               array_push($line, $allCode_s);
            }
            if ($crossDomain) {
               $allCode_s = '';
               if(is_array($entry->crossDomain))
               {
                  foreach($entry->crossDomain as $i => $code_s) {
                     if($i!=0) $allCode_s .= ',';
                     $allCode_s .= $code_s;
                  }
               }
               array_push($line, $allCode_s);
            }
            if ($valid) array_push($line, isset($currentValid));
            if ($docTitle && isset($infoAuthTitle[$entry->docid])) array_push($line, $infoAuthTitle[$entry->docid]['titles']);
            if ($docAuth && isset($infoAuthTitle[$entry->docid])) array_push($line, $infoAuthTitle[$entry->docid]['authors']);
            if ($domainList) {
               foreach($domainsL0 as $domainL0) {
                  if (is_array($entry->level0_domain_s)) {
                     array_push($line, in_array($domainL0, $entry->level0_domain_s)?'oui':'non');
                  } else {
                     array_push($line, 'non');
                  }
               }
            }
            fputcsv($output, $line, $separator = ";");
         }
      }
   }
} else {
   if($header) {
      $line = [];
      if ($countryCode) array_push($line, 'code pays');
      if ($countryName) array_push($line, 'nom pays');
      if ($selectInstitutionId) array_push($line, 'id institution');
      if ($institutionName) array_push($line, 'nom institution');
      if ($nbPub) array_push($line, 'nombre de publie');
      if ($docType) {
         foreach($docTypeList as $docType) {
            array_push($line, $docType);
         }
      }
      if ($valid) array_push($line, 'validité institution');
      if ($domainList) {
         foreach($domainsL0 as $domainL0) {
            $domainL0Doc = $domainSelector->code2doc($domainL0);
            $domainNameL0 = isset($domainL0Doc->fr_domain_s)?DomainSelector::cleanDomainName($domainL0Doc->fr_domain_s):$domainL0;
            array_push($line, $domainNameL0);
         }
      }
      fputcsv($output, $line, $separator = ";");
   }
   ksort($countryList);
   arsort($countryList);
   foreach($countryList as $country => $number){ 
      $thisCountryInstitutions=[];
      foreach($countryInstitutions[$country] as $institutionId){
         $thisCountryInstitutions[$institutionId] = intval($institutionList[$institutionId]??1);
      }
      arsort($thisCountryInstitutions);
      foreach($thisCountryInstitutions as $institutionId => $nb){
         $line = [];
         $countryNameValue = Countries::getCountry($country);
         if ($countryCode) array_push($line, $country);
         if ($countryName) array_push($line, $countryNameValue);
         if ($selectInstitutionId) array_push($line, $institutionId);
         if ($institutionName) array_push($line, $dbStruct->getStruct(intval($institutionId))['name']);
         if ($nbPub) array_push($line, $nb);
         if ($docType) {
            foreach($docTypeList as $docType) {
               if(isset($institutionDocTypeList[$institutionId])&&array_key_exists($docType, $institutionDocTypeList[$institutionId])) {
                  $nbDocType = $institutionDocTypeList[$institutionId][$docType];
               } else {
                  $nbDocType = 0;
               }
               array_push($line, $nbDocType);
            }
         }
         if ($valid) array_push($line, $dbStruct->getStruct($institutionId)['valid']);
         if ($domainList) {
            foreach($domainsL0 as $domainL0) {
               array_push($line, in_array($domainL0, $institutionDomainsL0[$institutionId])?'oui':'non');
            }
         }
         fputcsv($output, $line, $separator = ";");
      }
   }
}
?>