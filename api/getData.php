<?php
namespace uga\globhal\api;

use uga\globhal\data\DBStruct;
use uga\hallib\ref\domain\DomainSelector;

chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

/**
 * 
 * API pour l'obtension des données dans la partie domaine de résultat.
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

header('Content-Type: application/json');
header('Content-Encoding: UTF-8');
header('Content-type: application/json; charset=UTF-8');
echo "\xEF\xBB\xBF"; // UTF-8 BOM

$resultRequest = $_SESSION['resultRequest'];

$domainsL0 = $resultRequest["domainsL0"];
$countryList = $resultRequest["countryList"];
$domainCountryListL0 = $resultRequest["domainCountryListL0"];
$institutionList = $resultRequest["institutionList"];
$institutionDomainsL0 = $resultRequest["institutionDomainsL0"];

$dbStruct = new DBStruct();

$generateData = [];

if(isset($_REQUEST['datatable']) && $_REQUEST['datatable']) {
    $tableType = $_REQUEST['tableType'];

    // make header

    if ($tableType == 'country') {
        $generateData['headersTable'] = [
            'pays',
        ];
    } elseif ($tableType == 'institution') {
        $generateData['headersTable'] = [
            'id',
            'institution',
        ];
    }
    foreach($domainsL0 as $domainL0) {
        $domainNameL0 = isset($domainL0Doc->fr_domain_s)?DomainSelector::cleanDomainName($domainL0Doc->fr_domain_s):$domainL0;
        array_push($generateData['headersTable'], $domainNameL0);
    }

    // make table rows

    if ($tableType == 'country') {
        $generateData['rowsTable'] = [];
        foreach($countryList as $country) {
            $row = [$country];
            foreach($domainsL0 as $domainL0) {
                array_push($row, in_array($domainL0, $domainCountryListL0[$country])?'oui':'non');
            }
            array_push($generateData['rowsTable'], $row);
        }
    } elseif ($tableType == 'institution') {
        $generateData['rowsTable'] = [];
        foreach($institutionList as $institutionId => $nb) {
            $institutionName = $dbStruct->getStruct(intval($institutionId))['name'];
            $row = [$institutionId, $institutionName];
            foreach($domainsL0 as $domainL0) {
                array_push($row, in_array($domainL0, $institutionDomainsL0[$institutionId])?'oui':'non');
            }
            array_push($generateData['rowsTable'], $row);
        }
    }
}

echo json_encode($generateData);
