<?php
namespace uga\globhal\api;

chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

$_SESSION['tilemaps'] ??= file_get_contents("data/tilemaps.json");

header('Content-Type: application/json ');
header('Content-Encoding: UTF-8');
header('Content-type: application/json; charset=UTF-8');
header('Content-disposition: attachment;filename=tilemaps.json');
echo "\xEF\xBB\xBF"; // UTF-8 BOM

echo $_SESSION['tilemaps'];

