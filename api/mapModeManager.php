<?php
namespace uga\globhal\api;

/**
 * 
 * Permet de géré la liste des pluridisciplinarité supprimer.
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

if(isset($_REQUEST['mode'])) {
    $mode = $_REQUEST['mode'];
} else {
    $mode='crossDomain';
}

if(isset($_SESSION[$mode.'IgnorList'])) {
    $ignorList = $_SESSION[$mode.'IgnorList'];
} else {
    $ignorList = [];
}

$output = fopen('php://output', 'w');

if(isset($_GET['getIgnoreList'])&&$_GET['getIgnoreList']) {
    header('Content-Type: application/json ');
    header('Content-Encoding: UTF-8');
    header('Content-type: application/json; charset=UTF-8');
    header('Content-disposition: attachment;filename='.$mode.'IgnorList.json');
    echo "\xEF\xBB\xBF"; // UTF-8 BOM
    fwrite($output, json_encode($ignorList));
    exit();
}

if(isset($_POST['loadIgnoreList'])&&$_POST['loadIgnoreList']) {
    var_dump(json_decode($_POST['ignoreList']));
    $_SESSION[$mode.'IgnorList'] = json_decode($_POST['ignoreList']);
    exit();
}

if(isset($_POST['addToIgnore'])) {
    $addToIgnore = $_POST['addToIgnore'];
    foreach($addToIgnore as $ignoreElement) {
        array_push($ignorList, $ignoreElement);
    }
    $_SESSION[$mode.'IgnorList'] = $ignorList;
}

if(isset($_POST['removeFromIgnore'])) {
    $removeFromIgnore = $_POST['removeFromIgnore'];
    foreach($removeFromIgnore as $unignoreElement) {
        if (($key = array_search($unignoreElement, $ignorList)) !== false) {
            unset($ignorList[$key]);
        }
    }
    $_SESSION[$mode.'IgnorList'] = array_values($ignorList);
}
