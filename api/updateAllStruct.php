<?php
namespace uga\globhal\api;

use uga\globhal\data\DBStruct;

require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

/**
 * 
 * API mise à jour de toutes les structures.
 * 
 * @author Gaël PICOT
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
header('Content-Type: application/json ');
header('Content-Encoding: UTF-8');
header('Content-type: application/json; charset=UTF-8');
echo "\xEF\xBB\xBF"; // UTF-8 BOM

$dbStruct = new DBStruct();
$exculudeIncoming = ($_GET['exculudeIncoming']??'true') == 'true';
$updateOnly = ($_GET['updateOnly']??'true') == 'true';
echo json_encode($dbStruct->updateAllStruct($exculudeIncoming, $updateOnly));
