<?php
namespace uga\globhal\api;

use uga\hallib\ref\domain\DomainSelector;

/**
 * service to get domain list
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * Hallib :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';

header('Content-Type: application/json ');
header('Content-Encoding: UTF-8');
echo "\xEF\xBB\xBF"; // UTF-8 BOM
$domainSelector = new DomainSelector();
$domainSelector->extractData();

if(isset($_GET['level'])&&isset($_GET['parent'])) {
    $ar = isset($_GET['ar'])?$_GET['ar']:null;
    echo $domainSelector->generateJSONData($_GET['level'], $_GET['parent'], $ar);
}
