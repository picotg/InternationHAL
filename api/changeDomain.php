<?php
namespace uga\globhal\api;
chdir(dirname(__FILE__, 2));
require_once dirname(__FILE__, 2).DIRECTORY_SEPARATOR.'vendor/autoload.php';
session_name('globhal');
session_start();

use uga\globhal\query\QueryParameter;
use uga\globhal\query\QueryResult;

/**
 * 
 * API changement de domaine et réexecution de la requête courante.
 * 
 * @author Gaël PICOT
 * @author Philippe Gambette
 * 
 * GlobHAL :
 * Copyright (C) 2022 UGA
 * 
 * basée sur InternationHAL :
 * http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php
 * Copyright (C) 2017-2020 Philippe Gambette
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

header('Content-Type: application/json');
header('Content-Encoding: UTF-8');
header('Content-type: application/json; charset=UTF-8');
echo "\xEF\xBB\xBF"; // UTF-8 BOM

if(isset($_SESSION['parameter'])):
   $parameter =  new QueryParameter($_SESSION['parameter']);

   if(isset($_GET['domain'])){
      $parameter->domain=$_GET['domain'];
   }

   if(isset($_GET['level'])){
      $parameter->domainLevel=intval($_GET['level']);
   }
   $_SESSION['crossDomainIgnorList'] = [];
   $qr = new QueryResult($parameter);

   $qr->extractAllData();

   // enregistrer les résultats de la requête dans la session.
   $_SESSION['resultRequest'] = $qr->dataArray;
   $_SESSION['parameter'] = $parameter->toArray();
   $_SESSION['nbResult'] = $qr->getNbResult();
   $_SESSION['query'] = $qr->query;


   $data_resutlt = (isset($_SESSION['resultRequest']))?$_SESSION['resultRequest']:[];

   echo json_encode($data_resutlt);

endif;