# README GlobHal

Version modifier de InternationHAL (http://igm.univ-mlv.fr/~gambette/ExtractionHAL/InternationHAL.php)

## Principe de fonctionnement

La page d'accueil de l'application permet de faire une requête dans Hal et d'enregistré les différents résultats dans la session.

### Les pages

#### Requête (index.php)

La page principal pour la création et l'execution des requête.

#### Résultats

Les pages de résultats (result) permettes de voir, étudier et travailler sur les résultat.

- Carte (result/map.php) : affichage et manipulation de la carte [leaflet](https://leafletjs.com/)
- Domaines (result/domain.php) : affichage sous forme de [datatable](https://datatables.net/) et carte [leaflet](https://leafletjs.com/) des données liés au dommaines et pluridiciplinarité sans option de modification
- Listes institutions (result/classique.php) : affichage de la listes des institutions
- Datatable (result/datatable.php) : affichage de la liste compléte des données en utilisant [datatable](https://datatables.net/)
- Infos inconnues (result/unknowManager.php) : affichage et manipulation d'infos inconnues
- Export CSV (result/csv.php) : telechargement d'une version CSV des données avec shoix des option

#### Avancée

Paramétre et fonctionnalité avancé.

- Données (advanced/data.php) : gestion des données (enregistrement)
- Carte (advanced/map.php) : gestion des option de la carte (fond de carte et coulleur d'échelles)
- Export carte (advanced/export_map.php) : export de la carte pour l'intégré sur un autre site
- Administration (advanced/management.php) : Changement de la base de données Halto, mise à jours de la base de données des institutions et gestion des administrateurs

### Les APIs

Les APIs permettant de modifier et exécuter la requête sont :
- api/changeDomain.php : permet de changer le domaine courrant
- api/mapModeManager.php : permet de gérer les éléments (institution, domaines, etc.) ignorer.

Les APIs pour l'accés aux données de la requête courante sont :
- api/getData.php : accédé au donnée pour la page result/domain.php

### base de données des structures

Affin de réduire la taille et simplifier les requêtes, une base de données contenant les structures de type institution Hals (Hors INCOMMING par défault) est créé dans data/struct.db. Si un structure n'est pas présente dans la base de données alors elle est rechercher dans Hal puis ajouter à la base.

## Export de cartes

### Droit des fonds de cartes.

Attention, certaines cartes ont des copyright qui ne permette pas de  les afficher publiquement sur un site. Verifier le champs attribution dans le fichier data/tilemaps.json pour en savoir plus.

### Pour plus de fond de carte

https://leaflet-extras.github.io/leaflet-providers/preview/

# Fichiers et dossier

## /

- footer.php : pied de page
- index.php : page principale
- navbar.php : bar de navigation
- param.php : parametre de l'application (principalement CAS)

### advanced

pages contenant les fonctionnalités avancées

- advanced/data.php : fonctionnalité avancées pour voire les données en cours
- advanced/export_map.php : gestion de l'export des cartes
- advanced/management.php : gestion de la base Halto
- advanced/map.php : gestion des fond de cartes et des échelles

### api

- api/changeDomain.php : API changement de domaine et réexecution de la requête courante
- api/getData.php : API pour l'obtension des données dans la partie domaine de résultat
- api/institutionData.php : API pour accédé aux données des institutions
- api/updateAllStruct.php : scripte de mise à jour de la base de donnée des structure
- api/colorScales.php : manipulation et utilisation de la listes des couleurs pour échelles de la session courante
- api/csv.php : générateur de fichier CSV
- api/getDomain.php : service to get domain list en utilisant DomainSelector
- api/mapData.php : construction des données de la carte
- api/mapModeManager.php : gestion des éléments ignorer
- api/rowData.php : télécharger et renvoyer les données brutes
- api/tilemaps.php : manipulation et utilisation des infos des fonds de cartes

### data

- data/colorScales.json : couleurs pour échelles par défaut
- data/Countries.php : liste des pays avec leurs code
- data/DBStruct.php : gestion de la base de données des structures
- data/doctype.json : liste des type de document
- data/instance.json : liste des portails
- data/tilemaps.json : infos des fonds de cartes par défaut
- data/world-countries-no-antartica.json : GEOJSON représentant tous les pays pour la constructtion (source : https://www.datavis.fr/index.php?page=map-improve)

#### halto_db

bases de données halto

### dl

Téléchagement des fichier pour l'export de carte.

- dl/createzip.php : création de fichier zip pour l'export de cartes
- dl/MapExporter.php : outil pour la création de fichier zip avec option

### js

javascript

- js/halmap.js : gestion et création de la carte utilisant leaflet
- js/halmap.jqueryui.js : utilisation de [jQuery UI](https://jqueryui.com/) pour l'affichage des infos
- js/halmap.liquid.js : utilisation de [LiquidJS](https://liquidjs.com) pour l'affichage des infos
- js/parametricLink.js : gestion des parametre d'url pour le lien vers CSV
- js/test.scale.js : test de la gestion des échelles utilisant [mocha.js](https://mochajs.org/)

### query

création, execution et traitement des requêtes Hal.

- query/DataResult.php : Class abstraite pour la gestion d'une partie des résultats de la requête
- query/CountryDataResult.php : Implémentation de DataResult pour les pays
- query/DocTypeDataResult.php : Implémentation de DataResult pour les types de documents
- query/DomainDataResult.php : Implémentation de DataResult pour les domaines et les pluridisciplinarité
- query/EntryDataResult.php : Implémentation de DataResult pour les documents
- query/InstitutionDataResult.php : Implémentation de DataResult pour les institutions
- query/QueryParameter.php : Gestion des paramétres et construction des requêtes
- query/QueryResult.php : Résultat de la requête
- query/UnknownInfos.php : Festion des informations inconnu

### result

pages de la catégory résultat.

- result/classique.php : version originale de la présentation
- result/csv.php : page de configuration et de téléchargement de fichier CSV
- result/datatable.php : affichage en utilisant [datatable](https://datatables.net/)
- result/domain.php : affichage différente des domaines et des pluridisciplinarité
- result/map.php : affichage de la carte
- result/unknowManager.php : page de gestion des institutions inconnue

### test

test unitaire

#### hallib

test unitaire pour l'intégration de hallib

- test/hallib/QueryTest.php : test de requête

### views

Répertoire des templates [LiquidJS](https://liquidjs.com).

#### details

- view/details/institution.liquid : affichage des détails d'une institution dans la page result/classique.php

#### dialog

- view/dialog/collectionAdvencedSearch.liquid : dialog pour la recherche de collection dans Hal
- view/dialog/domainSelector.liquid : dialog pour selectionné un domaine pour restraindre la requête courante
- view/dialog/downloadMapData.liquid : dialog pour le téléchargement des données
- view/dialog/idhalAdvencedSearch.liquid : dialog pour la recherche d'un idHal
- view/dialog/loadDataFromBrowser.liquid : dialog pour l'enregistrement des requêtes
- view/dialog/structureAdvencedSearch.liquid : dialog pour la recherche de structure dans Hal
- view/dialog/uploadDataWaiting.liquid : dialog d'attente d'envoi de data
- view/dialog/waitRequest.liquid : dialog d'attente de fin de requête

#### domain

Template pour la page domaine des résultats

- view/domain/datatable.liquid : affichage de la table datatabel
- view/domain/page0.liquid : page dynamique pour l'affichage des info sur les domaine

#### infos

Template pour les infos bulles afficher sur la carte au passage sur un pays

- view/infos/infoCrossDomain.liquid : info sur les plurdisciplinarité
- view/infos/infoDefault.liquid : info par défaut (affiché quand la souris n'est sur aucun pays)
- view/infos/infoDomain.liquid : info sur les domaines
- view/infos/infoInstitution.liquid : info sur les institutions

#### list

Template pour l'affichage des liste complète sur la page carte

- view/list/crossDomainList.liquid : liste des plurdisciplinarité
- view/list/domainIgnoreList.liquid : liste des domaines ignorées
- view/list/domainList.liquid : liste des domaines
- view/list/institutionIgnoreList.liquid : liste des institutions ignorées
- view/list/institutionList.liquid : liste des institutions

#### popup

template pour l'affichage des popup quand un pays est clicker

- view/popup/popupCrossDomain.liquid : info sur les plurdisciplinarité
- view/popup/popupDomain.liquid : info sur les domaines
- view/popup/popupInstitution.liquid : info sur les institutions

#### unknow

template pour la gestion des institutions inconnues

- view/unknow/chouseCountry.liquid : dialog pour la selection d'un pays
- view/unknow/institutionInfo.liquid : template pour l'affichage des infos détail d'une institution chercher dans Hal à la demande
